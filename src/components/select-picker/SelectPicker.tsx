import classnames from "classnames";
import * as React from "react";
import Picker from "rmc-picker";
import { getTestId } from "../../utils/index";
import Button from "../button/Button";

import intlHoc, { IWrappedComProps } from '../intlHoc';

import "./SelectPicker.scss";

const cx = classnames

interface ISelectItemsState {
    [key: string]: {
        label: string;
    };
}

interface ISelectPickerProps extends IWrappedComProps {
    items: ISelectItemsState;
    selectedValue: string | number | null;
    onValueChange: (value: string) => void;
    onButtonClick: () => void;
    buttonText?: string;
    testId?: string;
}

class SelectPicker extends React.PureComponent<ISelectPickerProps> {
    onValueChange = (e: React.MouseEvent, key: string) => {
        e.stopPropagation();
        this.props.onValueChange(key);
    };
    render() {
        const { items, selectedValue, onValueChange, onButtonClick, buttonText, testId, t } = this.props;

        return (
            <div className={cx("select-picker")}>
                <div className={cx("picker-wrapper")}>
                    <div className={cx("picker-carousel")}>
                        <div className={cx("picker-mask")} />
                        <Picker
                            defaultSelectedValue={selectedValue}
                            selectedValue={selectedValue}
                            onValueChange={onValueChange}
                        >
                            {Object.entries(items)
                                .filter((item) => item[0] !== "placeholder")
                                .map(([key, value], index: number) => (
                                    <Picker.Item key={key} className={cx("picker-item")} value={key}>
                                        <div
                                            onClick={(e: React.MouseEvent) => this.onValueChange(e, key)}
                                            {...getTestId(`lbl-${testId}-item-${index}`)}
                                        >
                                            <span>{value.label}</span>
                                        </div>
                                    </Picker.Item>
                                ))}
                        </Picker>
                    </div>
                </div>
                <Button
                    className={cx("close-button")}
                    shadow={true}
                    block={true}
                    onClick={onButtonClick}
                    {...getTestId(`btn-${testId}-close`)}
                >
                    <span>{buttonText || t("done", "Done")}</span>
                </Button>
            </div>
        );
    }
}

export default intlHoc(SelectPicker);
