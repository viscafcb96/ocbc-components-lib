import cx from "classnames";
import * as React from "react";
import "./AlertMessage.scss";

interface IAlertMessageProps {
    /** Class to customize the alert container */
    className?: string;
}

const AlertMessage: React.FC<IAlertMessageProps> = (props) => (
    <div className={cx("common-alert-message", props.className)}>
        <img src={require("../../assets/images/shared/important.svg")} />
        <div className={cx("content")}>{props.children}</div>
    </div>
);

export default AlertMessage;
