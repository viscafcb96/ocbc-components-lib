const path = require('path');
const { resolve } = require('../utils/projectHelper');
const getWebpackConfig = require('../library/getWebpackConfig');
const webpackMerge = require('webpack-merge');
const babelConfig = require('../library/getBabelCommonConfig')(false);

const disableEsLint = e => {
    return (
        e.module.rules
            .filter(e => e.use && e.use.some(e => e.options && void 0 !== e.options.useEslintrc))
            .forEach(s => {
                e.module.rules = e.module.rules.filter(e => e !== s);
            }),
        e
    );
};

module.exports = async ({ config, mode }) => {
    //const cf = getWebpackConfig(false, mode);
    config = disableEsLint(config);

    config.module.rules = config.module.rules.filter(rule => !rule.test.test('.scss'));
    config.module.rules = config.module.rules.filter(rule => !rule.test.test('.tsx'));
    // Make whatever fine-grained changes you need
    config.module.rules.push({
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader'],
        include: path.resolve(__dirname, '../src'),
    });

    config.module.rules.push({
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000&name=webfonts/[name].[ext]',
    });
    config.module.rules.push({
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        loader: 'file-loader',
        options: {
            outputPath: 'webfonts',
        },
    });

    config.module.rules.push({
        test: /\.stories\.tsx?$/,
        loaders: [require.resolve('@storybook/source-loader')],
        enforce: 'pre',
    });

    config.module.rules.push({
        test: /\.tsx?$/,
        use: [
            {
                loader: resolve('babel-loader'),
                options: babelConfig,
            },
            {
                loader: resolve('ts-loader'),
                options: {
                    transpileOnly: true,
                },
            },
        ],
    });

    return webpackMerge({}, config);
};
