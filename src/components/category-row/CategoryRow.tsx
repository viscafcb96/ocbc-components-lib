import classnames from "classnames";
import * as React from "react";

import { getTestId } from "../../utils/index";

import "./CategoryRow.scss";
const cx = classnames

interface ICategoryRowProps {
    testId?: string;
    name: string;
    description?: string;
    image: string;
    percentage: number;
    categoryRowAmounts: React.ReactNode;
    budgetMode?: boolean;
    displayBorder?: boolean;
}

const MAX_PROGRESS_WIDTH_IN_PIXEL = 180;

const CategoryRow: React.FC<ICategoryRowProps> = ({
    testId,
    name,
    description,
    image,
    percentage,
    categoryRowAmounts,
    budgetMode = false,
    displayBorder = true,
}) => {
    const progressWidth = MAX_PROGRESS_WIDTH_IN_PIXEL * percentage;

    return (
        <div className={cx("category-row", "category-item", { "budget-mode": budgetMode, border: displayBorder })}>
            <div>
                <img {...getTestId(`img-${testId}-cat-row`)} src={image} className={cx("category-icon")} />
                <div>
                    <p {...getTestId(`lbl-${testId}-cat-row-title`)} className={cx("title")}>
                        <span>{name}</span>
                    </p>
                    {description && (
                        <p {...getTestId(`lbl-${testId}-cat-row-desc`)} className={cx("description")}>
                            <span>{description}</span>
                        </p>
                    )}
                </div>
            </div>
            <div {...getTestId(`${testId}-cat-row-progress`)}>
                {categoryRowAmounts}
                <div
                    style={{
                        width: `${progressWidth}px`,
                    }}
                    className={cx("progress")}
                />
            </div>
        </div>
    );
};

export default CategoryRow;
