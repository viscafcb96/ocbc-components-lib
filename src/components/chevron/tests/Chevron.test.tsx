import { shallow } from 'enzyme';
import * as React from 'react';
import BackChevron from '../Back';
import ForwardChevron from '../Forward';
import MenuChevron from '../Menu';

describe('Chevron', () => {
    it('renders the back chevron', () => {
        const template = shallow(<BackChevron />);
        expect(template.exists()).toBe(true);
    });

    it('simulates a back chevron tap', () => {
        const spy = jest.fn();
        const template = shallow(<BackChevron onClick={spy} />);
        template.simulate('click');
        expect(spy.mock.calls.length).toEqual(1);
    });

    it('renders the forward chevron', () => {
        const template = shallow(<ForwardChevron />);
        expect(template.exists()).toBe(true);
    });

    it('simulates a forward chevron tap', () => {
        const spy = jest.fn();
        const template = shallow(<ForwardChevron onClick={spy} />);
        template.simulate('click');
        expect(spy.mock.calls.length).toEqual(1);
    });

    it('renders the menu chevron', () => {
        const template = shallow(<MenuChevron />);
        expect(template.exists()).toBe(true);
    });

    it('simulates a menu chevron tap', () => {
        const spy = jest.fn();
        const template = shallow(<MenuChevron onClick={spy} />);
        template.simulate('click');
        expect(spy.mock.calls.length).toEqual(1);
    });
});
