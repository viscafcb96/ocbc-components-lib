import '@storybook/addon-storysource/register';
import '@storybook/addon-viewport/register';
import '@storybook/addon-actions/register';
import 'storybook-readme/register';
import '@storybook/addon-knobs/register';

import registerWithPanelTitle from 'storybook-readme/registerWithPanelTitle';

registerWithPanelTitle('Docs');
