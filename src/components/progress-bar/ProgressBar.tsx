import classnames from "classnames";
import * as React from "react";

import { moneyDisplayFormat } from "../../utils/helper/formatter";
import  "./ProgressBar.scss";
import { getTestId } from "../../utils";
import intlHoc, { IWrappedComProps } from '../intlHoc';
const cx = classnames;

export interface IProgressBarProps extends IWrappedComProps {
    current: number;
    end: number;
    color: string;
    marker?: boolean;
    label?: boolean;
    height?: number;
    testId?: string | undefined;
}

const computeLabelStyle = (current: number, end: number) => {
    const lessThanHalf = current / end > 0.5;
    const margin = lessThanHalf ? "7px auto 0 0" : "7px 0 0 auto";
    const textAlign = lessThanHalf ? ("right" as "right") : ("left" as "left");
    const width = lessThanHalf ? `${(current / end) * 100}%` : `${((end - current) / end) * 100}%`;

    return {
        margin,
        textAlign,
        width,
    };
};

const ProgressBar: React.FC<IProgressBarProps> = ({
    current,
    end,
    color,
    marker = false,
    label = false,
    height = 6,
    testId,
    t
}) => {
    const completed = current / end === 1;
    return (
        <div className={cx("progress-bar", color)} style={{ height }}>
            <div
                className={cx("current-progress", color, { marker: marker && !completed })}
                style={{
                    width: `${completed ? 100 : (current / end) * 100}%`,
                    height,
                }}
            />

            <div
                className={cx("current-progress-label", "small-regular", { label: label && !completed })}
                style={computeLabelStyle(current, end)}
                {...getTestId(testId && `lbl-${testId}-current-progress`)}
            >
                <span>
                    {moneyDisplayFormat(current)} {t("sgd", "SGD")}
                </span>
            </div>
        </div>
    );
};

export default intlHoc(ProgressBar);
