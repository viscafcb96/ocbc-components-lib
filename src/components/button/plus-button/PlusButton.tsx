import classnames from 'classnames';
import * as React from 'react';

import Button from '../Button';

const cx = classnames;

interface IPlusButtonProps {
    testId?: string;
    className?: string;
    onClick?: () => void;
}

const PlusButton = ({ testId, className, onClick }: IPlusButtonProps) => (
    <Button testId={`${testId}-plus`} onClick={onClick} className={cx('button', className)}>
        <i className={'fas fa-plus'} />
    </Button>
);

export default PlusButton;
