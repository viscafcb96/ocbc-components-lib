import cx from "classnames";
import * as React from "react";

import { getTestId } from "../../utils/index";
import Card from "./Card";
import "./Card.scss";

interface IGreyHeaderCardProps {
    testId?: string;
    headerText: string;
    className?: string;
    children: React.ReactNode;
}

const GreyHeaderCard: React.FC<IGreyHeaderCardProps> = ({ headerText, className, children, testId }) => {
    return (
        <Card className={cx("grey-header-card", className)} {...getTestId(`cards-grey-header-${testId}`)}>
            <div {...getTestId(`lbl-cards-grey-header-${testId}-title`)} className={cx("title", "xsmall-bold")}>
                <span>{headerText}</span>
            </div>
            {children}
        </Card>
    );
};

export default GreyHeaderCard;
