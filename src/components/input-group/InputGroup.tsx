import classnames from "classnames";
import React from "react";
import IntlHoc, { IWrappedComProps } from "../intlHoc";

import { getTestId } from '../../utils/index';
import Toggle from "../toggle/Toggle";

import "./InputGroup.scss";
const cx = classnames

export interface IOption {
    label: string | number | React.ReactElement;
    value: string | number;
    description?: React.ReactElement;
    disabled?: boolean | undefined;
    isAvailable?: boolean | undefined;
    defaultChecked?: boolean | undefined;
    error?: boolean | undefined;
    loading?: boolean | undefined;
    vAlignCenter?: boolean | undefined;
}

interface IInputGroup {
    label: string | number | React.ReactElement;
    value: string | number | object;
    checked: boolean;
    defaultChecked?: boolean;
}

interface IInputGroupProps extends IWrappedComProps {
    options: IOption[];
    name: string;
    type: string;
    testId?: string;
    disabled?: boolean | undefined;
    vAlignCenter?: boolean | undefined;
    className?: string | undefined;
    error?: boolean | undefined;
    onChange?: (event: Event | any, option: IInputGroup) => void;
}

interface IContentProps extends IWrappedComProps {
    option: IOption;
    type: string;
    disabled?: boolean | undefined;
    error?: boolean | undefined;
    onChange?: (event: Event | any, option: IInputGroup, id?: string | number) => void;
    index: number;
    name: string;
    vAlignCenter?: boolean | undefined;
    testId?: string;
}

const getContent = ({ option, type, onChange, disabled, error, index, name, vAlignCenter, testId, t }: IContentProps ) => {
    const {
        label,
        value,
        description,
        defaultChecked = false,
        vAlignCenter: vAlignCenterItem = false,
        disabled: disabledItem,
        isAvailable = true,
        error: errorItem,
        loading,
    } = option;

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { checked } = event.target;
        if (onChange) {
            onChange(event, { label, value, checked });
        }
    };

    const handleSwitchChange = (checked: boolean, id: string, event: Event) => {
        if (onChange) {
            onChange(event, { label, value, checked }, id);
        }
    };

    const getControl = () => {
        let control = null;
        if (isAvailable) {
            control =
                type === "toggle" ? (
                    <Toggle
                        checked={defaultChecked}
                        disabled={disabledItem || disabled}
                        loading={loading}
                        onHandleChange={handleSwitchChange}
                        className={cx("group-toggle")}
                        id={`${testId}-${index}`}
                    />
                ) : (
                    <input
                        {...getTestId(`input-${testId}-${index}`)}
                        value={value}
                        {...defaultChecked && { defaultChecked }}
                        name={name}
                        type={type}
                        disabled={disabled || disabledItem}
                        className={cx({
                            error: error || errorItem,
                        })}
                        onChange={handleChange}
                    />
                );
        } else {
            control = t("not-available", "Not Available");
        }
        return control;
    };

    return (
        <div
            className={cx("input-group-item", "label-wrapper", {
                checked: defaultChecked,
                na: !isAvailable || disabledItem || disabled,
            })}
            key={index}
            {...getTestId(`${testId}-${index}-label-wrapper`)}
        >
            <label
                className={cx("label", {
                    "v-align-center": vAlignCenterItem || vAlignCenter,
                })}
            >
                <span>{label}</span>
                {getControl()}
            </label>
            {description}
        </div>
    );
};

const InputGroup: React.FC<IInputGroupProps> = (props: IInputGroupProps) => {
    const { options, name, type, disabled, className, onChange, error, vAlignCenter, testId, t } = props;
    return (
        <div className={cx("input-group", type, className)} {...getTestId(testId)}>
            {options.map((option: IOption, index: number) =>
                getContent({ option, type, onChange, disabled, error, index, name, vAlignCenter, testId, t })
            )}
        </div>
    );
};

export default IntlHoc(InputGroup);
