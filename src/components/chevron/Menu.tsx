import cx from 'classnames';
import * as React from 'react';
import './style/index.scss';

interface IMenuChevron {
    light?: boolean;
    active?: boolean;
    transparent?: boolean;
    soft?: boolean;
    dark?: boolean;
    onClick?: (e: React.MouseEvent) => void;
}

export default class MenuChevron extends React.Component<IMenuChevron & React.HTMLProps<HTMLButtonElement>> {
    render() {
        const { active, light, transparent, className, soft, dark, ...rest } = this.props;
        return (
            <button
                {...rest}
                className={cx('menu', 'far fa-chevron-down', { light, active, transparent, soft, dark }, className)}
                type="button"
                onClick={this.props.onClick}
            />
        );
    }
}
