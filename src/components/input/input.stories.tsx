import * as React from 'react';

import Input from './Input';

export default { title: 'Input' };


export const Default = () => <Input placeholder="Access Code" onChange={e => console.log((e.target as HTMLInputElement).value)} />

export const PasswordInput = () => <Input placeholder="PIN" type="password" />

export const WithError = () => <Input placeholder="PIN" value="1234" error="This is an error" />

export const Disabled = () => <Input placeholder="Disabled Input" disabled={true} />