import { mount, shallow } from 'enzyme';
import * as React from 'react';

import AmountInput from '../AmountInput';

const props = {
    value: 1,
    onAmountChange: () => null,
};

describe('AmountInput', () => {
    it('renders the AmountInput component', () => {
        const template = shallow(<AmountInput {...props} />);
        expect(template.exists()).toBe(true);
    });

    it('Displays the error message when there is an error prop', () => {
        const template = shallow(<AmountInput {...props} error={true} errorMessage={'This is an error message'} />);
        expect(template.find('.error-message').text()).toBe('This is an error message');
    });

    it('Returns the correct color for the lineColor method', () => {
        const templateError = shallow(
            <AmountInput {...props} error={true} errorMessage={'This is an error message'} />,
        );
        const instanceError = templateError.instance() as AmountInput;
        expect(instanceError.lineColor()).toEqual('error');

        const templateFocused = mount(<AmountInput {...props} />);
        templateFocused.find('.input-wrapper').simulate('click');
        const instanceFocused = templateFocused.instance() as AmountInput;
        expect(instanceFocused.lineColor()).toEqual('focused');

        const templateValue = shallow(<AmountInput {...props} />);
        const instanceValue = templateValue.instance() as AmountInput;
        expect(instanceValue.lineColor()).toEqual('light');

        const template = shallow(<AmountInput {...props} value={0} />);
        const instance = template.instance() as AmountInput;
        expect(instance.lineColor()).toEqual('light');
    });

    it('Focuses on the input element when the wrapper is clicked', () => {
        const template = mount(<AmountInput {...props} />);
        template.find('.input-wrapper').simulate('click');
        expect(template.find('.real-input').getDOMNode()).toEqual(document.activeElement);
    });
});
