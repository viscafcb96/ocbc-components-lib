import { shallow } from 'enzyme';
import * as React from 'react';
import Button from '../Button';

describe('Button', () => {
    it('renders primary', () => {
        const template = shallow(<Button primary={true} />);
        expect(template.find('button').hasClass('primary')).toBe(true);
    });

    it('renders block', () => {
        const template = shallow(<Button block={true} />);
        expect(template.find('button').hasClass('block')).toBe(true);
    });

    it('renders text', () => {
        const template = shallow(<Button text={true} />);
        expect(template.find('button').hasClass('text')).toBe(true);
    });

    it('renders outline', () => {
        const template = shallow(<Button outline={true} />);
        expect(template.find('button').hasClass('outline')).toBe(true);
    });

    it('renders shadow', () => {
        const template = shallow(<Button shadow={true} />);
        expect(template.find('button').hasClass('shadow')).toBe(true);
    });

    it('renders children', () => {
        const template = shallow(<Button>test</Button>);
        expect(template.find('button').text()).toBe('test');
    });

    it('simulates a button tap', () => {
        const spy = jest.fn();
        const template = shallow(<Button onClick={spy} />);
        template.find('button').simulate('click');
        expect(spy.mock.calls.length).toEqual(1);
    });
});
