import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import * as React from 'react';

import SelectPicker from './SelectPicker';

const items = {
    January: {
        label: "January"
    },
    February: {
        label: 'February'
    },
    March: {
        label: 'March'
    },
    April: {
        label: 'April'
    },
    May: {
        label: 'May'
    },
    June: {
        label: 'June'
    },
    July: {
        label: 'July'
    },
    August: {
        label: 'August'
    },
    September: {
        label: 'September'
    },
    test: {
        label: 'test'
    },
    October: {
        label: 'October'
    },
    November: {
        label: 'November'
    },
    December: {
        label: 'December'
    }
}

storiesOf('SelectPicker', module).add('default', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <SelectPicker
           items={items}
           seletedValue={"test"}
           onValueChange={(value: string) => console.log(value)}
           onButtonClick={() => console.log("on button click")}
        />
    </div>
));
