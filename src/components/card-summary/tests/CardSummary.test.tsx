import { shallow } from "enzyme";
import * as React from "react";
import CardSummary from "../CardSummary";

describe("CardSummary", () => {
    const props = {
        status: "",
        icon: "",
        cardName: "",
        cardNumber: "",
    };

    it("renders the card summary component", () => {
        const wrapper = shallow(<CardSummary {...props} />);
        expect(wrapper.exists()).toBe(true);
    });
});
