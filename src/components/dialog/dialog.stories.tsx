import * as React from 'react';

import { InternalDialog } from './Dialog';

const defaultProps = {
    title: "Hello",
};

export default { title: 'Dialog' };

export const Dialog = () => <InternalDialog isOpen={true} onRequestClose={() => console.log('on request close')}><div className="hello" />Hello Dialog</InternalDialog>

