const SCREEN_WIDTH = window.screen.width;
const SCREEN_HEIGHT = window.screen.height;
const SCREEN_RATIO = window.screen.width / window.screen.height;

enum EScreenRatios {
    IPHONE_5 = 320 / 568,
    IPHONE_6 = 375 / 667,
    IPHONE_6_PLUS = 414 / 736,
    IPHONE_XS = 375 / 812,
}

enum EScreenHeights {
    IPHONE_5 = 568,
    IPHONE_6 = 667,
    IPHONE_6_PLUS = 736,
    IPHONE_XS = 812,
}

declare let window: any;

const isIOS = () => {
    if (window.cordova) {
        return window.cordova.platformId === 'ios';
    }
    return false;
};

let originalClientHeight = 0;

document.addEventListener('DOMContentLoaded', () => {
    if (document.readyState === 'interactive' || document.readyState === 'complete') {
        originalClientHeight = document.documentElement.clientHeight;
    }
});

const isKeyboardOpen = () => {
    return document.documentElement.clientHeight < originalClientHeight;
};

export { SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_RATIO, EScreenRatios, EScreenHeights, isIOS, isKeyboardOpen };
