import { addParameters, configure, addDecorator } from '@storybook/react';
import '@storybook/addon-console';
import '@storybook/addon-viewport/register';
import { withKnobs } from '@storybook/addon-knobs';
import { addReadme } from 'storybook-readme';
import viewports from '../src/utils/viewport';

addParameters({
    viewport: {
        viewports: viewports, // newViewports would be an ViewportMap. (see below for examples)
        defaultViewport: 'iphone6',
    },
});
addDecorator(withKnobs);
addDecorator(addReadme);
configure(require.context('../src/components', true, /\.stories\.(tsx|mdx)$/), module);
