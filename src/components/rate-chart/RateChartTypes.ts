export interface IRateChartProps {
    xLabels: string[];
    values: number[];
    xTickAmount: number;
    tooltipFormat?: (() => string) | null | undefined;
    xLabelsFormat?: (() => string) | null | undefined;
    value?: string;
    label?: string;
}

export interface IRateChartState {
    b: string;
}
