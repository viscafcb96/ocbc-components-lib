/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/interface-name-prefix */
import classnames from 'classnames';
import Tooltip from 'rc-tooltip';
import * as React from 'react';
import './rc-tooltip.css';
// import 'rc-tooltip/assets/bootstrap_white.css';

import './Tooltip.scss';
const cx = classnames;

interface ITooltipWrapper {
    /** Contents of what will appear in the Tooltip */
    content: JSX.Element[] | JSX.Element | string;
    /** Placement of the Tooltip relative to what it is anchored to */
    placement: string;
    visible?: boolean;
    classname?: string;
    reactRef?: any;
}

const TooltipWrapper: React.FC<ITooltipWrapper> = props => {
    const { content, children, placement, visible, classname = '' } = props;
    const arrowClass = placement.includes('top') ? 'tooltip-wrapper-top' : 'tooltip-wrapper-bottom';

    return (
        <Tooltip
            overlay={content}
            visible={visible}
            defaultVisible={visible}
            placement={placement}
            overlayClassName={cx('tooltip-wrapper', arrowClass, classname)}
        >
            <>{children}</>
        </Tooltip>
    );
};

export default TooltipWrapper;
