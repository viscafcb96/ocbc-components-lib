import { shallow } from "enzyme";
import * as React from "react";
import Toggle from "../Toggle";

describe("Toggle", () => {
    it("renders the toggle button", () => {
        const wrapper = shallow(
            <Toggle checked={true} onHandleChange={(checked: boolean, id: string) => console.log(checked, id)} />
        );
        expect(wrapper.exists()).toBe(true);
    });

    it("handleChange", () => {
        const handleChangeFn = jest.fn();
        const wrapper = shallow(
            <Toggle checked={true} onHandleChange={(checked: boolean, id: string) => handleChangeFn()} />
        );
        const instance = wrapper.instance() as Toggle;
        instance.handleChange(true, null, "id");
        expect(handleChangeFn).toHaveBeenCalled();
    });
});
