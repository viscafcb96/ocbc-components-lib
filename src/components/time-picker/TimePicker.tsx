import classnames from "classnames";
import * as React from "react";
import Picker from "rmc-picker";
import MultiPicker from "rmc-picker/lib/MultiPicker";
import Button from "../button/Button";

import intlHoc, { IWrappedComProps } from '../intlHoc';


import {
    formatDisplayTime,
    generateTimeArray,
    getArrayFromTime,
    getValidTimePickerValue,
    ITimer,
    Meridiem,
} from "../../utils/helper/dateTime";

import "./TimePicker.scss";

const cx = classnames

interface ITimePickerProps extends IWrappedComProps {
    selectedValue: number[];
    onValueChange: (value: number[]) => void;
    is12HourFormat?: boolean;
    onButtonClick?: () => void;
    buttonText?: string;
}

const TimePicker: React.FC<ITimePickerProps> = (props) => {
    const { onValueChange, selectedValue, onButtonClick, buttonText, is12HourFormat, t } = props;
    const [selectedHours, selectedMinutes, selectedMeridiem] = selectedValue;
    const onPickerValueChange = (draftTimerArr: number[]) => {
        const [hours, minutes, meridiem] = draftTimerArr;
        onValueChange(getArrayFromTime({ hours, minutes, meridiem }, is12HourFormat));
    };

    const onTap = (e: React.MouseEvent, tapValue: number, index: number) => {
        e.stopPropagation();
        const updatedSelectedTime = [...selectedValue];
        updatedSelectedTime[index] = tapValue;
        onValueChange(getValidTimePickerValue(updatedSelectedTime, selectedValue, is12HourFormat, index));
    };

    return (
        <div className={cx("time-picker")}>
            <div className={cx("picker-wrapper")}>
                <div className={cx("picker-carousel")}>
                    <div className={cx("picker-mask")} />
                    <MultiPicker
                        selectedValue={getArrayFromTime({
                            hours: selectedHours,
                            minutes: selectedMinutes,
                            meridiem: selectedMeridiem,
                        })}
                        onValueChange={onPickerValueChange}
                    >
                        {generateTimeArray(is12HourFormat).map((item: number[], parentIndex: number) => (
                            <Picker key={"parent" + parentIndex}>
                                {item.map((i: number, childIndex: number) => (
                                    <Picker.Item key={"child" + childIndex} value={i} className={cx("picker-item")}>
                                        <div onClick={(e: React.MouseEvent) => onTap(e, i, parentIndex)}>
                                            {formatDisplayTime(i, parentIndex, is12HourFormat)}
                                        </div>
                                    </Picker.Item>
                                ))}
                            </Picker>
                        ))}
                    </MultiPicker>
                </div>
            </div>
            <Button className={cx("close-button")} shadow={true} block={true} onClick={onButtonClick}>
                {buttonText || t("done", "Done")}
            </Button>
        </div>
    );
};

export default intlHoc(TimePicker);
