import cx from 'classnames';
import * as React from 'react';

// import { getTestId } from '../../utils/app-utils';
import PlusButton from './plus-button/PlusButton';

import './style/Button.scss';

interface IButtonProps {
    /** ??????????? */
    active?: boolean; // Test of comments
    /** Indicates if the button is enabled or disabled */
    disabled?: string | boolean | undefined;
    /** ??????????? */
    block?: string | boolean | undefined;
    /** Indicates if the button should be styled with the primary color */
    primary?: string | boolean | undefined;
    /** ??????????? */
    text?: string | boolean | undefined;
    /** ??????????? */
    outline?: string | boolean | undefined;
    /** ??????????? */
    shadow?: string | boolean | undefined;
    /** ??????????? */
    light?: string | boolean | undefined;
    testId?: string;
    /** light background button */
    lightSolid?: string | boolean | undefined;
}

class Button extends React.Component<IButtonProps & React.HTMLProps<HTMLButtonElement>, {}> {
    static Plus: typeof PlusButton;
    handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        if (typeof this.props.onClick === 'function' && this.props.disabled !== true) {
            this.props.onClick(e);
        }
    };
    render() {
        const {
            active,
            disabled,
            block,
            primary,
            text,
            outline,
            shadow,
            light,
            lightSolid,
            onClick,
            className,
            testId,
            id,
            children,
            ...rest
        } = this.props;
        return (
            <div className={`btn-${testId}`}>
                <button
                    {...rest}
                    className={[
                        cx('common-btn', {
                            [cx('active')]: active,
                            [cx('disabled')]: disabled,
                            [cx('block')]: block,
                            [cx('primary')]: primary,
                            [cx('text')]: text,
                            [cx('outline')]: outline,
                            [cx('shadow')]: shadow,
                            [cx('light')]: light,
                            [cx('light-solid')]: lightSolid,
                        }),
                        className,
                    ].join(' ')}
                    type="button"
                    onClick={this.handleClick}
                    disabled={disabled}
                >
                    {children}
                </button>
            </div>
        );
    }
}

Button.Plus = PlusButton;
export default Button;
