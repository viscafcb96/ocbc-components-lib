import * as React from 'react';

import ChevronButton from './ChevronButton';

export default { title: 'ChevronButton' };

export const Default = () => (
    <ChevronButton><h1>Default ChevronButton</h1></ChevronButton>
);
