import classnames from 'classnames/bind';
import React, { TouchEvent, useRef, useState } from 'react';
// import styles from './SwipeButton.scss';
import './SwipeButton.scss';
const cx = classnames;

export interface ISwipeButtonProps {
    name?: string;
    type?: string;
    onClick?: () => void;
    width?: string;
    onTouchStart?: () => void;
    onEndStart?: () => void;
    onSwiping?: () => void;
    onSuccessSwipe?: () => void;
    text?: string;
    disable?: boolean;
}

const SwipeButton: React.FunctionComponent<ISwipeButtonProps> = ({
    width,
    disable,
    onTouchStart = () => null,
    onEndStart = () => null,
    onSwiping = () => null,
    onSuccessSwipe = () => null,
    text = 'Slide to do something',
}) => {
    const [successSwipe, successSwipeUpdate] = useState(false);
    const [mouseIsDown, mouseIsDownUpdate] = useState(false);
    const [initialMouse, initialMouseUpdate] = useState(0);
    const [slideMovementTotal, slideMovementTotalUpdate] = useState(0);

    const sliderRef = useRef<HTMLDivElement>(null);
    const buttonRef = useRef<HTMLDivElement>(null);
    const loadingRef = useRef<HTMLImageElement>(null);
    const arrows1Ref = useRef<HTMLElement>(null);
    const arrows2Ref = useRef<HTMLElement>(null);
    const slideTextRef = useRef<HTMLSpanElement>(null);

    const onSliderClick = successSwipe
        ? () => {
              if (
                  !sliderRef.current ||
                  !buttonRef.current ||
                  !loadingRef.current ||
                  !arrows1Ref.current ||
                  !arrows2Ref.current
              ) {
                  return;
              }
              if (!sliderRef.current.classList.contains(cx('unlocked'))) {
                  return;
              }
              sliderRef.current.classList.remove(cx('unlocked'));
              buttonRef.current.classList.remove(cx('bg-converted'));
              loadingRef.current.style.display = 'none';
              arrows1Ref.current.style.display = 'flex';
              arrows2Ref.current.style.display = 'flex';
              successSwipeUpdate(false);
          }
        : () => {
              console.log('');
          };

    const onTouch = (event: React.MouseEvent<HTMLElement> | TouchEvent) => {
        if (
            !sliderRef.current ||
            !buttonRef.current ||
            !loadingRef.current ||
            !arrows1Ref.current ||
            !arrows2Ref.current
        ) {
            return;
        }
        onTouchStart();
        buttonRef.current.classList.add(cx('bg-on-click'));
        mouseIsDownUpdate(true);
        slideMovementTotalUpdate(buttonRef.current.clientWidth - sliderRef.current.clientWidth - 4);
        // @ts-ignore
        initialMouseUpdate(event.clientX || event.touches[0].pageX);
    };

    const onMouseUp = (event: React.MouseEvent<HTMLElement> | TouchEvent) => {
        if (
            !sliderRef.current ||
            !buttonRef.current ||
            !loadingRef.current ||
            !slideTextRef.current ||
            !arrows1Ref.current ||
            !arrows2Ref.current
        ) {
            return;
        }
        if (!mouseIsDown) {
            return;
        }
        mouseIsDownUpdate(false);

        // @ts-ignore
        const currentMouse = event.clientX || event.changedTouches[0].pageX;
        const relativeMouse = currentMouse - initialMouse;

        if (relativeMouse < slideMovementTotal) {
            slideTextRef.current.style.opacity = '1';
            sliderRef.current.style.transition = 'opacity 300ms';
            setTimeout(() => {
                if (!sliderRef.current) {
                    return;
                }
                sliderRef.current.style.transition = '';
            }, 300);

            sliderRef.current.style.left = '4px';
            sliderRef.current.style.transition = 'left 300ms';
            setTimeout(() => {
                if (!sliderRef.current) {
                    return;
                }
                sliderRef.current.style.transition = '';
            }, 300);

            buttonRef.current.classList.remove(cx('bg-on-click'));
            onEndStart();
            return;
        }

        buttonRef.current.classList.add(cx('bg-converted'));
        sliderRef.current.classList.add(cx('unlocked'));

        setTimeout(() => {
            if (!loadingRef.current || !arrows1Ref.current || !arrows2Ref.current) {
                return;
            }
            loadingRef.current.style.display = 'flex';
            arrows1Ref.current.style.display = 'none';
            arrows2Ref.current.style.display = 'none';
        }, 250);

        successSwipeUpdate(true);
        setTimeout(() => onSuccessSwipe(), 1000);
    };

    const onMouseMove = (event: React.MouseEvent<HTMLElement> | TouchEvent) => {
        if (
            !sliderRef.current ||
            !buttonRef.current ||
            !slideTextRef.current ||
            !loadingRef.current ||
            !arrows1Ref.current ||
            !arrows2Ref.current
        ) {
            return;
        }
        if (!mouseIsDown) {
            return;
        }
        // @ts-ignore
        const currentMouse = event.clientX || event.touches[0].pageX;
        const relativeMouse = currentMouse - initialMouse;
        const slidePercent = 1 - relativeMouse / slideMovementTotal;

        slideTextRef.current.style.opacity = slidePercent.toString();
        //
        if (relativeMouse <= 0) {
            sliderRef.current.style.left = '4px';
            return;
        }
        if (relativeMouse >= slideMovementTotal - 4) {
            sliderRef.current.style.left = slideMovementTotal + 'px';
            return;
        }
        sliderRef.current.style.left = relativeMouse + 'px';
        onSwiping();
    };

    return (
        <div
            ref={buttonRef}
            id="button-background"
            className={[
                cx('bg-button', {
                    [cx('disable')]: disable,
                }),
            ].join(' ')}
            style={{ width }}
        >
            <span ref={slideTextRef} className={cx('slide-text')}>
                {text}
            </span>

            <div
                id="slider"
                ref={sliderRef}
                className={cx('slider-button')}
                onTouchStart={onTouch}
                onMouseDown={onTouch}
                onTouchEnd={onMouseUp}
                onMouseUp={onMouseUp}
                onMouseMove={onMouseMove}
                onTouchMove={onMouseMove}
                onClick={onSliderClick}
            >
                <img
                    ref={loadingRef}
                    className={cx('loading')}
                    id="swipe-loading"
                    src="./images/Spinner.svg"
                    alt="slider"
                />
                <i ref={arrows1Ref} id="locker" className={cx('material-icons ', 'md-arrow', 'custome-arrow')}>
                    chevron_right
                </i>
                <i ref={arrows2Ref} id="locker-2" className={cx('material-icons ', 'md-arrow', 'custome-arrow')}>
                    chevron_right
                </i>
            </div>
        </div>
    );
};

export default SwipeButton;
