import * as React from 'react';
import { SlideToUnlock } from './SlideToUnlock';

export default { title: 'SlideToUnlock' };

export const Default = () => <div style={{ width: "400px", height: "55px" }}>
    <SlideToUnlock
        onUnlock={() => console.log("unlocked")}
        onChange={(progress: any) => console.log("progress is ", progress)}
    />
</div>

