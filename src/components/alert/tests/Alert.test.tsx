import { shallow } from "enzyme";
import * as React from "react";
import Alert from "../Alert";

describe("Alert", () => {
    it("renders the tick", () => {
        const template = shallow(<Alert withTick={true} />);
        expect(template.hasClass("tick")).toBe(true);
    });

    it("simulates a button tap", () => {
        const spy = jest.fn();
        const template = shallow(<Alert onClick={spy} />);
        template.simulate("click");
        expect(spy.mock.calls.length).toEqual(1);
    });
});
