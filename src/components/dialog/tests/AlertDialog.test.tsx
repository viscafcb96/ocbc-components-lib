import { mount, shallow } from "enzyme";
import React from "react";

import Button from "../../button/Button";
import { AlertDialogContent } from "../AlertDialog";
import AlertIcon, { EAlertIconType } from "../AlertIcon";

const defaultProps = {
    title: "Hello",
};

describe("Dialog", () => {
    it("render component", () => {
        const tpl = shallow(
            <AlertDialogContent {...defaultProps}>
                <div className="hello" />
            </AlertDialogContent>
        );

        expect(tpl.exists()).toBe(true);
    });

    it("render HTML children as HTML", () => {
        const tpl = shallow(
            <AlertDialogContent {...defaultProps}>
                <div />
            </AlertDialogContent>
        );

        expect(
            tpl
                .find(".content")
                .children()
                .first()
                .type()
        ).toEqual("div");
        expect(tpl.find(".content-not-align").exists()).toBe(true);
    });

    it("render string children as string", () => {
        const tpl = shallow(<AlertDialogContent {...defaultProps}>{"hello"}</AlertDialogContent>);

        expect(tpl.find(".content").text()).toEqual("hello");
        expect(tpl.find(".content-not-align").exists()).toBe(false);
    });

    it("render provided buttons and trigger respective handlers", () => {
        const props = {
            ...defaultProps,
            primaryBtn: {
                label: "Test",
                onClick: jest.fn(),
            },
            secondaryBtn: {
                label: "Test",
                onClick: jest.fn(),
            },
            counterBtn: {
                label: "Test",
                onClick: jest.fn(),
            },
        };

        const tpl = mount(
            <AlertDialogContent {...props}>
                <div className="hello" />
            </AlertDialogContent>
        );

        tpl.find("button")
            .at(0)
            .simulate("click");
        expect(props.primaryBtn.onClick).toHaveBeenCalled();

        tpl.find("button")
            .at(1)
            .simulate("click");
        expect(props.secondaryBtn.onClick).toHaveBeenCalled();

        tpl.find("button")
            .last()
            .simulate("click");
        expect(props.counterBtn.onClick).toHaveBeenCalled();
    });

    it("render icon when given one", () => {
        const props = {
            ...defaultProps,
            icon: <AlertIcon type={EAlertIconType.ERROR} />,
        };

        const tpl = mount(
            <AlertDialogContent {...props}>
                <div className="hello" />
            </AlertDialogContent>
        );

        expect(
            tpl
                .find(AlertIcon)
                .first()
                .exists()
        ).toBe(true);
    });
});
