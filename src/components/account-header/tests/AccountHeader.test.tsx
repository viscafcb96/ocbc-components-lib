// import { mount, shallow } from 'enzyme';
// import * as React from 'react';
// import BackChevron from '../../chevron/Back';
// import Settings from '../../settings/Settings';
// import AccountHeader from '../AccountHeader';

// describe('AccountHeader', () => {
//     it('does not render the BackChevron component', () => {
//         const template = shallow(<AccountHeader title={'Test'} accountNumber={'Number'} />);
//         expect(template.find(BackChevron).exists()).toBe(false);
//     });

//     it('show correct title and account number', () => {
//         const template = mount(<AccountHeader title={'Test-title'} accountNumber={'1000-00'} />);
//         expect(template.find('.account').text()).toBe('Test-title');
//         expect(template.find('.account-number').text()).toBe('1000-00');
//     });

//     it('does not render the Settings component by default', () => {
//         const template = shallow(<AccountHeader title={'test'} accountNumber={'12345'} />);
//         expect(template.find(Settings).exists()).toBe(false);
//     });

//     it('renders the Settings component if the settings prop is false', () => {
//         const template = shallow(<AccountHeader title={'test'} accountNumber={'12345'} settings={true} />);
//         expect(template.find(Settings).exists()).toBe(false);
//     });
// });
