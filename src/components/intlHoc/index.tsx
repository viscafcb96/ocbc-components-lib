import * as React from 'react';
import { injectIntl } from 'react-intl';
 
export interface IValuesIntl {
    [k: string]: any
}
export interface IWrappedComProps {
    t?: (id: string, defaultMessage: string, values?: IValuesIntl,) => string,
}

const intlHoc = (WrappedCom: React.ComponentType<IWrappedComProps>) => {
    return class WithIntl extends React.Component<any, any> {
        t = (id: string, defaultMessage: string, values : IValuesIntl = {}) => {
            const { intl } = this.props
            return intl ? intl.formatMessage({ id, values, defaultMessage }) : defaultMessage
        }
        render() {
            return (
                <WrappedCom {...this.props} t={this.t}  />
            )
        }
    }
}

export default intlHoc