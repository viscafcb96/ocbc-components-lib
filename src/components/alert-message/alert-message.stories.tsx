import * as React from 'react';

import AlertMessage from './AlertMessage';

export default { title: 'AlertMessage' };

export const Default = () => (
    <AlertMessage>
        <h3>This is an alert!</h3>
    </AlertMessage>
);

