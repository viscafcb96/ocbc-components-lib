import cx from "classnames";
import * as React from "react";

import "./Alert.scss";

interface IAlertProps {
    active?: boolean;
    withTick?: boolean;
}

const Alert: React.FC<IAlertProps & React.HTMLProps<HTMLButtonElement>> = (props) => {
    const { active, withTick, disabled, ...rest } = props;
    return (
        <button
            {...rest}
            className={cx("common-button-alert", "fal fa-bell", { tick: withTick, active, disabled })}
            type="button"
        />
    );
};

export default Alert;
