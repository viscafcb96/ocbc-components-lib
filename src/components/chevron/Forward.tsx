import cx from 'classnames';
import * as React from 'react';
import './style/index.scss';

interface IChevron {
    active?: boolean;
    light?: boolean;
    dark?: boolean;
    transparent?: boolean;
    highlight?: boolean;
    onClick?: () => void;
}

export default class ForwardChevron extends React.Component<IChevron & React.HTMLProps<HTMLButtonElement>> {
    render() {
        const { active, light, dark, transparent, highlight, className, ...rest } = this.props;
        return (
            <button
                {...rest}
                className={cx(
                    'forward',
                    'far fa-chevron-right',
                    { active, light, dark, highlight, transparent },
                    className,
                )}
                type="button"
                onClick={this.props.onClick}
            />
        );
    }
}
