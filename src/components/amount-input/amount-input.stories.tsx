import * as React from 'react';

import AmoutInputStory from './index';

export default { title: 'Amount Input' };

export const Default = () => (
    <AmoutInputStory labelText="Amount Input demo" value={30000} currency="SGD" onAmountChange={e => console.log(e)}></AmoutInputStory>
);

export const AutoFocus = () => (
    <AmoutInputStory autoFocus labelText="Amount Input demo" value={30000} currency="SGD" onAmountChange={e => console.log(e)}></AmoutInputStory>
);

export const WithHelper = () => (
    <AmoutInputStory helper helperMessage="This is helper message" labelText="Amount Input demo" value={30000} currency="SGD" onAmountChange={e => console.log(e)}></AmoutInputStory>
);
