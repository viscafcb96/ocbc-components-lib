import cx from "classnames";
import React from "react";

import "./AlertIcon.scss";

export enum EAlertIconType {
    SUCCESS = "alert-icon-success",
    INFORMATION = "alert-icon-information",
    WARNING = "alert-icon-warning",
    ERROR = "alert-icon-error",
}

interface IAlertIconProps {
    type: EAlertIconType;
}

const AlertIcon: React.FC<IAlertIconProps> = ({ type }) => (
    <div className={cx("alert-icon-wrapper", type)}>
        <div
            className={cx("fas", {
                "fa-check": type === "alert-icon-success",
                "fa-exclamation": type === "alert-icon-information" || type === "alert-icon-warning",
                "fa-times": type === "alert-icon-error",
            })}
        />
    </div>
);

export default AlertIcon;
