import { isKeyboardOpen } from './device-utils';

const INPUT_FOCUS_TRANSLATABLE_CONTAINER = 'transition-wrapper';

const watchKeyboardClose = () => {
    const WATCH_KEYBOARD_INTERVAL = 100;

    const watchKeyboardCloseInterval = setInterval(() => {
        if (!isKeyboardOpen()) {
            const appContainer = document.getElementById(INPUT_FOCUS_TRANSLATABLE_CONTAINER)!;
            appContainer.style.transform = '';
            appContainer.style.height = '';
            clearInterval(watchKeyboardCloseInterval);
        }
    }, WATCH_KEYBOARD_INTERVAL);
};

export const onInputFocus = (ref: HTMLElement | HTMLInputElement | null) => {
    const WATCH_KEYBOARD_INTERVAL = 100;
    const KEYBOARD_MARGIN_SCREEN_RATIO = 0.06;

    if (ref) {
        // Translate the whole component to top to allow the field to be always visible
        // For these we need to wait until we find a difference between document.documentElement.clientHeight
        // and window.screen.height, which means the keyboard is open.
        let latestHeight = 0;

        const watchSizeChangeInterval = setInterval(() => {
            if (isKeyboardOpen() && latestHeight === window.screen.height) {
                const appContainer = document.getElementById(INPUT_FOCUS_TRANSLATABLE_CONTAINER)!;
                const minKeyboardMarginTop = window.screen.height * KEYBOARD_MARGIN_SCREEN_RATIO;
                const targetBottom = ref.getBoundingClientRect().bottom;
                const pageHeight = document.documentElement.clientHeight;

                if (pageHeight - targetBottom < minKeyboardMarginTop) {
                    const translateY = targetBottom - pageHeight + minKeyboardMarginTop;
                    appContainer.style.transform = `translateY(${-translateY}px)`;
                    appContainer.style.height = `calc(100vh + ${translateY}px)`;
                    watchKeyboardClose();
                }

                clearInterval(watchSizeChangeInterval);
            }

            latestHeight = window.screen.height;
        }, WATCH_KEYBOARD_INTERVAL);
    }
};


export const getTestId = (id?: string) => ({ id, "aria-label": id });