import * as React from 'react';

import Alert from './Alert';

export default { title: 'Alert' };

export const Active = () => (
    <Alert active onClick={() => console.log("Alert onClick event")}></Alert>
);

export const withTick = () => (
    <Alert withTick onClick={() => console.log("Alert onClick event")}></Alert>
);
