import classnames from "classnames";
import * as React from "react";
import Switch from "react-switch";
import omit from 'omit.js';

import { getTestId } from "../../utils/index";

import "./Toggle.scss";
const cx = classnames

interface IToggleProps {
    checked: boolean;
    loading?: boolean;
    onHandleChange: any;
    disabled?: boolean;
    className?: string;
    id?: string | number;
    offColor?: string;
}

interface IToogleStates  {
    checked?: boolean
}

class Toggle extends React.Component<IToggleProps, IToogleStates> {
    constructor(props: IToggleProps) {
        super(props)
        this.state = {
            checked: this.props.hasOwnProperty('checked') ? this.props.checked : false
        }
    }
    handleChange = (checked: boolean, event: any, id: string) => {
        this.setState({ checked })
        this.props.onHandleChange(checked, id, event);
    };

    render() {
        const {
            loading = false,
            disabled,
            className,
            id,
            offColor = "#fff",
            onHandleChange,
            ...rest
        } = this.props;
        const { checked } = this.state;

        return (
            <Switch
                checked={checked}
                onChange={this.handleChange}
                disabled={disabled || loading}
                offColor={offColor}
                onColor="#19b6a0"
                handleDiameter={26}
                uncheckedIcon={false}
                checkedIcon={false}
                boxShadow={"0 2px 10px 0 rgba(0, 0, 0, 0.2)"}
                activeBoxShadow={"0 2px 10px 0 rgba(0, 0, 0, 0.2)"}
                height={32}
                width={50}
                className={cx("toggle", { checked, loading }, className ? className : "default")}
                {...id && getTestId(`btn-${id.toString()}`)}
                {...omit(rest, ['checked'])}
            />
        );
    }
}

export default Toggle;
