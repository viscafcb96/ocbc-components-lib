import cx from "classnames";
import * as React from "react";

import "./Card.scss";

import { getTestId } from "../../utils/index";

interface ICard {
    unit?: boolean;
    cardRef?: any;
    className?: string;
    testId?: string;
}

const Card: React.FC<ICard & React.HTMLProps<HTMLDivElement>> = (props) => {
    const { unit, className = "", cardRef, testId, ...rest } = props;

    return (
        <div
            {...getTestId(`${testId}-card`)}
            {...rest}
            ref={cardRef}
            className={cx("common-card", { [className]: className, unit })}
        >
            {props.children}
        </div>
    );
};

export default Card;
