import * as React from 'react';

import Card from './Card';
import GreyHeaderCard from './GreyHeaderCard';

export default { title: 'Card' };

export const Default = () => (
    <Card><h1>Card default</h1></Card>
);

export const withUnit = () => (
    <Card unit><h1>Card with unit</h1></Card>
);

export const GreyHeader = () => (
    <GreyHeaderCard headerText="This is header text"><h1>This is content</h1></GreyHeaderCard>
);
