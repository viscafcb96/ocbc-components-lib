// import classnames from 'classnames/bind';
// import { Options } from 'highcharts';
// import moment from 'moment';
// import React from 'react';
// import ReactHighcharts from 'react-highcharts';
//
// import { IRateChartProps, IRateChartState } from './RateChartTypes';
//
// import './RateChartScreen.scss';
// const cx = classnames;
//
// const tooltipFormatterDefault = function() {
//     // @ts-ignore
//     return `<div class='${cx('tooltip')}'><p>${this.y}</p><p>${moment(this.key).format(
//         'DD MMM YYYY, HH:MM',
//     )}</p></div>`;
// };
//
// const xLabelsFormatterDefault = function() {
//     // @ts-ignore
//     const position = this.pos === 0 ? this.pos : this.pos - 1;
//     // @ts-ignore
//     const value = this.chart.axes[0].categories[position];
//     return moment(value).format('HH:MM');
// };
//
// class RateChart extends React.Component<IRateChartProps, IRateChartState> {
//     render() {
//         const { xLabels, values, xTickAmount, tooltipFormat, xLabelsFormat } = this.props;
//         const xLabelInterval = values.length / (xTickAmount - 1);
//         const tooltipFormatter = tooltipFormat ? tooltipFormat : tooltipFormatterDefault;
//         const xLabelsFormatter = xLabelsFormat ? xLabelsFormat : xLabelsFormatterDefault;
//         const avgValue = values.reduce((a, b) => a + b, 0) / values.length;
//
//         const defaultConfig: Options = {
//             chart: {
//                 // @ts-ignore
//                 style: { maxHeight: '50vh', width: 'calc(100vw - 30px)', textAlign: 'center !important' },
//                 marginTop: 100,
//                 marginBottom: 0,
//                 marginLeft: 0,
//                 marginRight: 0,
//                 backgroundColor: 'transparent',
//                 // backgroundColor: {
//                 //     linearGradient: [0, 0, 0, 500],
//                 //     stops: [[0, "#fff"], [1, "#d1d8db"]],
//                 // },
//             },
//             title: {
//                 text: null,
//             },
//             legend: { enabled: false },
//             plotOptions: {
//                 series: {
//                     marker: {
//                         enabled: false,
//                         fillColor: '#FFFFFF',
//                         lineWidth: 1,
//                         lineColor: '#21d2a3',
//                     },
//                 },
//             },
//             // @ts-ignore
//             tooltip: {
//                 borderWidth: 0,
//                 borderColor: 'transparent',
//                 borderRadius: 0,
//                 backgroundColor: 'transparent',
//                 shadow: false,
//                 useHTML: true,
//                 formatter: tooltipFormatter,
//             },
//             // @ts-ignore
//             xAxis: {
//                 tickInterval: xLabelInterval,
//                 categories: xLabels,
//                 labels: {
//                     useHTML: true,
//                     align: 'center',
//                     style: {
//                         fontSize: '2.933vw',
//                     },
//                     formatter: xLabelsFormatter,
//                     y: 25,
//                 },
//                 tickLength: 7,
//                 endOnTick: true,
//                 startOnTick: true,
//             },
//             // @ts-ignore
//             yAxis: {
//                 title: '',
//                 tickAmount: 3,
//                 tickWidth: 1,
//                 // tickLength: 40,
//                 tickColor: '#dce2e5',
//                 gridLineColor: '#dce2e5',
//                 labels: {
//                     y: -5,
//                     x: 0,
//                     align: 'left',
//                     useHTML: true,
//                     style: {
//                         fontSize: '2.933vw',
//                     },
//                 },
//                 minRange: avgValue * 2,
//             },
//             series: [
//                 {
//                     color: '#319988',
//                     name: '1D',
//                     data: values,
//                     // @ts-ignore
//                     marker: {
//                         symbol:
//                             'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAGVUlEQVRYhc1ZW2xUVRRd+9wznZkOLQOFlldbCgKSFiQhaVTCQ2ICiYI/gH74YyLhhxCjwhfxw4AfEIwxxsT4Y4wfRGJiQBJIFASjUUIUlKfYNm0p9EHb6bsz956zzb4z0xamzDClPFY6yUzP2Wuve8+5++y9L53m00jDQwKzEktQPFwK1xlGv+pBqReFAWGIDRy28AIhkDLQrqm1TCug7CKwmgNGFEA4RTUEQgxkb8GqG4r4ghdwzrF1oN1hGFIIkwMHjHYdwxQ7FQETQm+oHa0F16FRMKJJIw8wUYUy3lYyWG+ZagAUwTgA8SCABACTYhPfBbBOIYhhmfqUy5cY3ikmOgKg6UG9ZhXIACj5KQbwDqC2kbWVAHWD0AWgHcSUYZgGcZomAMvLCLwGpN4C+FsCPiGglzOM7obK+M8YKAKG2bySYP6RQHt8R0RXQWj3d0RSfy6QP1dsxBYICJdwCrfKwXBfgWIXNlP2uoxvDHgOAZcB9GazeQCIba9wCadwi49sGsddYjFgg880B962ZP8DMODvq8mB0DsEtBCpiPLUXkt2FgE7x2PXA77vJBgemCyI1acAtluyl1Mbf7LEjYVwDlmyV8DYTlCWye4a9gOAOypwIRaM/BjwhkCJwG4P7g4Q0uKyrcDDgnwfhMseuzsoEWieTeUHIzo8QktsRp+j7gF3XZvXd1Q7aCLQ0CMWNxbM4LBnUFGmizZPiwR+HhHY0BNLfmMutIQzDJ4NoOURLWs2yGrNJdBtxVgLIomtUC48yCdBdqcFVz8hcUj5bBENoiWti+piHbKQMxnOaT/OJUPJ41raeyH7TQ4Fl2BeAqNDxcNyWhW8TmyrAPQ8QXFI+e4RLaJJtKkAxYms3cigrocMwpMFJVpEk2jTOmFqDHMNiDon4kDW5PDNhlln+7qiVwe7Q3L+VhdFh9cUl8W2lVW0Tmg5iDqJbY2T4Bptra0FO1NB3JHv8nZaE9h98Y9FZ7vbwtCEqqJikAOc7GqOnOxuipzqa53+0cLlN0oo5GYYZ4cLVlMt21rNyj4Lq1mSqawm90Du3J5rl5acvdUYXFk+Hx8sXobV00v9a/wl1or9zX/hp46G8N6Qu+Tzeasu5XknSUI0k1miYfQckB3MmJIDhxsbys401wdXzK3Ery9uRFCNbt/NMyqwoWQeVv39PU513g5+V9hYtmV6ZVt2xnsl2kEYPVdYo6lkMy8c7+wogVL4uHrlXeLSCJLCoaoXYLTBicHGkowJuSGaoiqVppt8rev6+4MVRVPxfHRmxlgatVNKsbgwinqvN5gxmBuiKTxuujV54ORfftv7Lii/wJnA0bYgUhRv6uvB77E7GWNpnOvvwL/9MSwMFMczBnPDT8dEoGQLBflavzpjRieMxbuXzyNubcZ4nC3er/sdytPYUFgxkRgrmmIKjtcCVoUZwznwRmVV29ryBfELNxux6reTONrWjG4v4X+O3mnC6j9/wPnWdqyfMTu+pSTPJ1ggmhyvRRM71zkZAzmfQC0TDyytub47PrjobHtr+LWuVlQVF4MUUD+UTOHWl84f2l++/MYEdqAfl0UbNfR3LTOGjwEUS1Vq+TLhcHPDrDN9XdFrA90hOUmXFkWH1xWVxbbNnuBR59dKHHUc2kQ3Ej2EYXuMrH0ORPkvxaMAcxk76iKCapNyOcis1AkCTweQudsfP6xoYVInRNvTn7AaIimrOgz4awbKn/BdtKJBtPiaiJ7+ounpLzuv8pWRWVK4B+Olu6d40X2G3MdRuCO17xyHA9X9OrY3Hmy/q3DXdagfnak9VPK0g9oLlBt+LN0FTvUSqzUCX3CBe/C2bgaNaRnpCCIjP6TDSqykP7NLEgnF6lE0j9KQC48oVs9Ysl8y7C7xHUI4d4fVP/Mc7PTgtsKj94jUACUfHDUJd5NTT+tcZhux2uxz4Ozj+2SkmanwGJYhp39fgPCmA7rFQHUqRj1MGBLbYuESTuEWH9m6rPcViNRlhsg5XkD0MoMPJKstXgpGaeruZ+NOg/25YiO2gCtcwincNgfDuEucRjrF4eTp8iFgv2LlbCVgPaSWlia6JELjNdEln2Pym+gA+qDoHwadIjZHAGpK8ebcL3ml/MTcZLQ+RGQOaWNrrZXXEGYRcJ/XEMqMvobQ6hyzvIbwpDB/MIcA/gefMvDFLjNviwAAAABJRU5ErkJggg==)',
//                     },
//                 },
//             ],
//             credits: { text: '' },
//         };
//
//         return <ReactHighcharts config={defaultConfig} />;
//     }
// }
//
// export default RateChart;
