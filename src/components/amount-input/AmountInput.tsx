/* eslint-disable @typescript-eslint/no-unused-vars */
import cx from 'classnames';
import * as React from 'react';
import { onInputFocus } from '../../utils/';

import { moneyDisplayFormat } from '../../utils/helper/formatter';

import Line from '../line';
import './style/index.scss';

interface IAmountInputProps {
    value: number;
    error?: boolean;
    errorMessage?: string;
    helper?: boolean;
    helperMessage?: string;
    labelText?: string;
    currency?: string;
    onAmountChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    decimalPlaces?: number;
    autoFocus?: boolean;
    onBlur?: () => void;
    scrollOffset?: number;
    testId?: string;
    autoScroll?: boolean;
}

interface IAmountInputState {
    focused: boolean;
    increment: boolean;
}

export default class AmountInput extends React.Component<
    IAmountInputProps & React.HTMLProps<HTMLInputElement>,
    IAmountInputState
> {
    state = {
        focused: false,
        increment: true,
    };

    realInput = React.createRef<HTMLInputElement>();

    componentDidMount() {
        if (this.props.autoFocus) {
            this.onFocus();
        }
    }

    handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Backspace') {
            this.setState({ increment: false });
        } else if (e.key.match(/\d/)) {
            this.setState({ increment: true });
        }
    };

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { decimalPlaces } = this.props;
        let value: string = e.target.value;
        while (value[0] === '0') {
            value = value.slice(1);
        }

        if (decimalPlaces) {
            if (value.length === 1) {
                const divideBy = Math.pow(10, decimalPlaces);
                value = (+value / divideBy).toFixed(decimalPlaces);
            } else {
                if (this.state.increment) {
                    value = (+value * 10).toFixed(decimalPlaces);
                } else {
                    value = (+value / 10).toFixed(decimalPlaces);
                }
            }
        }

        let maxLength = 9;
        if (decimalPlaces && value.includes('.')) {
            maxLength += decimalPlaces + 1; // length includes the "."
        }

        if (value.length > maxLength && this.state.increment) {
            return;
        }

        e.target.value = value;
        this.props.onAmountChange(e);
    };

    onFocus = () => {
        const { decimalPlaces, value, autoScroll = true } = this.props;
        this.realInput.current!.value = '';
        this.realInput.current!.value = decimalPlaces ? value.toFixed(decimalPlaces) : value.toString();
        this.realInput.current!.focus();
        this.setState({ focused: true });

        autoScroll && onInputFocus(this.realInput.current);
    };

    handleBlur = () => {
        const { onBlur } = this.props;

        this.setState({ focused: false });
        if (onBlur) {
            onBlur();
        }
    };

    lineColor = () => {
        if (this.props.error) {
            return 'error';
        }
        if (this.state.focused) {
            return 'focused';
        }
        return 'light';
    };

    displayValue = () =>
        this.props.value >= 0 ? moneyDisplayFormat(this.props.value, this.props.decimalPlaces || 0) : 0;

    displayCurrency = () => (typeof this.props.currency === 'string' ? this.props.currency : 'sgd');

    render() {
        const {
            value,
            labelText,
            currency,
            onAmountChange,
            error,
            errorMessage,
            helper,
            helperMessage,
            decimalPlaces,
            style,
            onBlur,
            onFocus,
            testId,
            ...rest
        } = this.props;

        return (
            <div className={cx('common-amount-input')}>
                <div className={cx('label-text', 'small-regular')}>{labelText}</div>
                <div className={cx('input-wrapper')} onClick={this.onFocus} style={style}>
                    <div className={cx('display-formatted-value')}>{this.displayValue()}</div>
                    <input
                        className={cx('real-input')}
                        type="number"
                        pattern="\d*"
                        ref={this.realInput}
                        value={value}
                        onChange={this.handleChange}
                        onKeyDown={this.handleKeyPress}
                        onBlur={this.handleBlur}
                        onFocus={onFocus}
                        {...rest}
                    />
                    <div className={`lbl-${testId}-currency`}>
                        <span>{this.displayCurrency()}</span>
                    </div>
                </div>
                <Line color={this.lineColor()} />
                {error && (
                    <div className={cx('error-message', 'small-regular', `lbl-${testId}-error`)}>
                        <span>{errorMessage}</span>
                    </div>
                )}
                {helper && (
                    <div className={cx('helper-message', 'small-regular', `lbl-${testId}-helper`)}>
                        <span>{helperMessage}</span>
                    </div>
                )}
            </div>
        );
    }
}
