import classnames from 'classnames';
import * as React from 'react';

// import { getTestId } from '../../utils/app-utils';
import './style/index';

const cx = classnames;

interface IAddBox {
    testId?: string;
    label: string;
    onClick: () => void;
}

const AddBox = ({ label, onClick }: IAddBox) => {
    return (
        <div className={cx('add-box')} onClick={onClick}>
            <div className={cx('picto-add')}>
                <div className={cx('fas fa-plus')} />
            </div>
            <span>{label}</span>
        </div>
    );
};

export default AddBox;
