import cx from "classnames";
import React from "react";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";

import { getTestId } from "../../utils";

import "./SlideToUnlock.scss";

interface ISlideToUnlockProps {
    testId?: string;
    /** text to display inside the slider */
    text?: string;
    /** on unlock event handler */
    onUnlock: () => void;
    /** onSlide event handler */
    onChange?: (delta: number) => void;
}

interface ISlideToUnlockSate {
    handleDistanceWithOrigin: number;
}

const UNLOCK_PERCENTAGE = 0.75;
// Size of the handle with padding;
const HANDLE_SIZE = 50;

const POSITION_DECREMENT = 20;

export class SlideToUnlock extends React.Component<ISlideToUnlockProps, ISlideToUnlockSate> {
    static defaultProps = {
        text: "",
    };

    state = {
        handleDistanceWithOrigin: 0,
    };

    readonly trackRef = React.createRef<HTMLDivElement>();
    readonly textRef = React.createRef<HTMLParagraphElement>();

    start: number | null = null;

    getTrackWidth = () => {
        if (!this.trackRef.current) {
            return 0;
        }
        return this.trackRef.current.getBoundingClientRect().width - HANDLE_SIZE;
    };

    animateTextOpacity = (delta: number) => {
        const opacity = 1 - delta;
        if (this.textRef.current) {
            this.textRef.current.style.opacity = `${opacity}`;
        }
    };

    animateSliderToOrigin = () => {
        this.setState(
            (prevState) => ({
                handleDistanceWithOrigin: Math.max(prevState.handleDistanceWithOrigin - POSITION_DECREMENT, 0),
            }),
            this.animateHandleAndTextOpacity
        );
    };

    animateHandleAndTextOpacity = () => {
        const delta = this.state.handleDistanceWithOrigin / this.getTrackWidth();
        this.animateTextOpacity(delta);
        if (this.props.onChange) {
            this.props.onChange(delta);
        }
        if (this.state.handleDistanceWithOrigin > 0) {
            window.requestAnimationFrame(this.animateSliderToOrigin);
        }
    };

    onDragStop = (_e: DraggableEvent, data: DraggableData) => {
        const trackWidth = this.getTrackWidth();
        const delta = data.x / trackWidth;
        if (delta < UNLOCK_PERCENTAGE) {
            window.requestAnimationFrame(this.animateSliderToOrigin);
        } else {
            this.setState({ handleDistanceWithOrigin: trackWidth });
            if (this.props.onChange) {
                this.props.onChange(1);
            }
            this.props.onUnlock();
        }
    };

    onDrag = (_e: DraggableEvent, data: DraggableData) => {
        this.setState({
            handleDistanceWithOrigin: data.x,
        });
        const delta = data.x / this.getTrackWidth();
        this.animateTextOpacity(delta);
        if (this.props.onChange) {
            this.props.onChange(delta);
        }
    };

    render() {
        const { handleDistanceWithOrigin } = this.state;
        const { text, testId } = this.props;
        return (
            <div className={cx("slide-to-unlock", "track")} ref={this.trackRef}>
                <p {...getTestId(`lbl-${testId}-slide-unlock`)} ref={this.textRef} className={cx("text")}>
                    <span>{text}</span>
                </p>
                <Draggable
                    axis="x"
                    defaultPosition={{ x: handleDistanceWithOrigin, y: 0 }}
                    bounds={"parent"}
                    scale={1}
                    onDrag={this.onDrag}
                    position={{ x: handleDistanceWithOrigin, y: 0 }}
                    onStop={this.onDragStop}
                >
                    <div className={cx("handle", "fas fa-angle-right")} />
                </Draggable>
            </div>
        );
    }
}
