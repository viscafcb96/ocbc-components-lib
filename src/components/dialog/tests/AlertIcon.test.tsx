import { shallow } from "enzyme";
import React from "react";

import AlertIcon, { EAlertIconType } from "../AlertIcon";

describe("AlertIcon", () => {
    for (const type of Object.keys(EAlertIconType)) {
        it(`render icon of type ${type}`, () => {
            const tpl = shallow(<AlertIcon type={type as EAlertIconType} />);
            expect(tpl.find(`.${type}`).exists()).toBe(true);
        });
    }
});
