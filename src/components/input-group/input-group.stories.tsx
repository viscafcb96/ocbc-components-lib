import * as React from 'react';

import InputGroup from './InputGroup';

export default { title: 'InputGroup' };
const props = {
    name: "inputGroup",
    options: [
        { label: "360 Account", value: "360 Account", defaultChecked: true },
        { label: "Statement Savings Account", value: "Statement Savings Account", defaultChecked: false },
    ],
};

export const Radio = () => (
    <InputGroup type="radio" onChange={(_: any, ...rest: any) => console.log(rest)} {...props} />
);

export const Checkbox = () => (
    <InputGroup type="checkbox" onChange={(_: any, ...rest: any) => console.log(rest)} {...props} />
);

export const Toggle = () => (
    <InputGroup type="toggle" onChange={(_: any, ...rest: any) => console.log(rest) } {...props} />
);