import { shallow } from 'enzyme';
import * as React from 'react';
import AddBox from '../AddBox';

describe('AddBox', () => {
    it('renders the box', () => {
        const handleClick = jest.fn();
        const template = shallow(<AddBox label={'label'} onClick={handleClick()} />);
        expect(template.hasClass('add-box')).toBe(true);
        template.simulate('click');
        expect(handleClick).toHaveBeenCalled();
    });
});
