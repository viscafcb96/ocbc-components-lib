import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import * as React from 'react';
import ProgressBar from './ProgressBar';

export default { title: "ProgressBar" }

interface IPBRunnerState {
    current: number
}

interface IPBRunnerProps {
    end: number
    label?: boolean;
    marker?: boolean
    color?: string
}

class ProgressBarRunner extends React.Component<IPBRunnerProps, IPBRunnerState> {
    protected runnerInterval: number;
    constructor(props: any) {
        super(props)
        this.state = {
            current: 0
        }
    }
    componentDidMount() {
        this.runnerInterval = window.setInterval(() => {
            console.log('current', this.state.current)
            if(this.state.current < this.props.end) {
                this.setState({current: this.state.current + 1})
            } else this.runnerInterval && window.clearInterval(this.runnerInterval);
        }, 500)
    }
    componentWillUnmount() {
        this.runnerInterval && window.clearInterval(this.runnerInterval);
    }
    render() {
        const { current } = this.state
        const { end, marker, label, color } = this.props
        return (
            <div
                style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "center",
                }}
            >
                <ProgressBar label={label} marker={marker} current={current} end={end} height={60} color={color} />
            </div>
        )
    }
}

storiesOf('ProgressBar', module).add('Default', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <ProgressBarRunner end={100} color="green"/>
    </div>
));

storiesOf('ProgressBar', module).add('WithLabel', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <ProgressBarRunner end={100} color="grey" label />
    </div>
));

storiesOf('ProgressBar', module).add('withMarker', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <ProgressBarRunner end={100} marker label color="orange"/>
    </div>
));
