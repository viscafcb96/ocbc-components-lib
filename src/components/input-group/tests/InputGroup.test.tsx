import { shallow } from "enzyme";
import React from "react";
import InputGroup from "../InputGroup";

const props = {
    name: "inputGroup",
    options: [
        { label: "360 Account", value: "360 Account" },
        { label: "Statement Savings Account", value: "Statement Savings Account" },
    ],
};

describe("Input Group", () => {
    it("renders input group", () => {
        const template = shallow(<InputGroup type="radio" {...props} />);
        expect(template.exists()).toBe(true);
    });
    it("renders radio button", () => {
        const template = shallow(<InputGroup type="radio" {...props} />);
        expect(template.find(".radio")).toBeDefined();
    });
    it("renders checkbox", () => {
        const template = shallow(<InputGroup type="checkbox" {...props} />);
        expect(template.find(".checkbox")).toBeDefined();
    });
});
