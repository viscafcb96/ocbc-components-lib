import cx from "classnames";
import * as React from "react";

import "./CardToggles.scss";

interface ICardTogglesProps {
    data: any;
}

const CardToggles: React.FC<ICardTogglesProps> = (props) => {
    const { data } = props;

    return (
        <div className={cx("card-toggles")}>
            {data.map((card: any, index: number) => (
                <div key={index} >{card.name}</div>
            ))}
        </div>
    );
};

export default CardToggles;
