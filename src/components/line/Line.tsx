import classnames from 'classnames';
import * as React from 'react';

import './style/index.scss';

const cx = classnames;

interface ILineProps {
    color?: string; // accepts (focused, error, light, dark)
    className?: string;
}

// common Line component used for cards
const Line: React.FC<ILineProps> = props => {
    const { color = '', className = '' } = props;
    return <hr className={cx('line', className, color)} />;
};

export default Line;
