import cx from 'classnames';
import * as React from 'react';
import './style/index.scss';

interface IChevron {
    active?: boolean;
    light?: boolean;
    dark?: boolean;
    blue?: boolean;
    select?: boolean;
    transparent?: boolean;
    highlight?: boolean;
    onClick?: () => void;
}

const DownChevron: React.FC<IChevron & React.HTMLProps<HTMLButtonElement>> = ({
    active,
    light,
    dark,
    blue,
    select,
    transparent,
    highlight,
    className,
    onClick,
    ...rest
}) => (
    <button
        {...rest}
        className={cx(
            'down',
            'far fa-chevron-down',
            { active, light, dark, blue, select, highlight, transparent },
            className,
        )}
        type="button"
        onClick={onClick}
    />
);

export default DownChevron;
