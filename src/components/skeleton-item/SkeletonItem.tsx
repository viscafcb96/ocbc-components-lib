import classnames from "classnames";
import * as React from "react";

import  "./SkeletonItem.scss";

const cx = classnames

interface ISkeletonItem {
    width: number | string;
    height: number | string;
    mt?: number;
    mb?: number;
    mr?: number;
    ml?: number;
    radius?: number | string;
    dark?: boolean;
}

const SkeletonItem = ({ width, height, mt, mb, mr, ml, radius, dark }: ISkeletonItem) => {
    const buildValue = (value: number | string) => (typeof value === "string" ? value : `${value}px`);
    return (
        <div
            className={cx(["skeleton-item", dark && "dark"])}
            style={{
                width: buildValue(width),
                height: buildValue(height),
                marginTop: `${mt}px`,
                marginBottom: `${mb}px`,
                marginRight: `${mr}px`,
                marginLeft: `${ml}px`,
                borderRadius: radius ? buildValue(radius) : 0,
            }}
        />
    );
};

export default SkeletonItem;
