import { mount, shallow } from "enzyme";
import * as React from "react";
import { simulateMovementFromTo } from "../../../utils/test-utils/touchEvents";
import { SlideToUnlock } from "./SlideToUnlock";

const props = {
    onUnlock: () => null,
    onChange: (delta: number) => null,
};

describe("SlideToUnlock", () => {
    it("render without failing", () => {
        const wrapper = shallow(<SlideToUnlock {...props} />);
        expect(wrapper.exists()).toBe(true);
    });
    it("call onChange when slider is being dragged", () => {
        const onChangeMock = jest.fn();
        const handle = mount(<SlideToUnlock {...props} onChange={onChangeMock} />)
            .find(".handle")
            .first();
        simulateMovementFromTo(handle, 0, 0, 10, 0);
        expect(onChangeMock).toBeCalled();
    });
    it("call onUnlock when slider is being unlocked", () => {
        const onUnlockMock = jest.fn();
        const handle = mount(<SlideToUnlock {...props} onChange={onUnlockMock} />)
            .find(".handle")
            .first();
        const toX = handle.getDOMNode().clientWidth;
        simulateMovementFromTo(handle, 0, 0, toX, 0);
        expect(onUnlockMock).toBeCalled();
    });
});
