import * as React from "react";
import classnames from "classnames";
import intlHoc, { IWrappedComProps } from '../intlHoc';
import { getTestId } from "../../utils";
import { getArrayFromDate, MONTH } from "../../utils/helper/dateTime";

import "./SelectDate.scss";
const cx = classnames

interface ISelectDateProps extends IWrappedComProps {
    date: number;
    selectLabel?: string;
    onClick: () => void;
    testId?: string;
    isFocused: boolean;
}

const SelectDate: React.FC<ISelectDateProps> = ({ date, selectLabel, onClick, isFocused, testId, t }) => {
    const [day, month, year] = getArrayFromDate(date);

    const dayMonthDisplayValue = (num: number, isMonth = false) => {
        if (isNaN(num)) {
            return isMonth ? "MMM" : "DD";
        }

        if (isMonth && MONTH[num]) {
            return t(MONTH[num].short, MONTH[num].short);
        }

        return num < 10 ? `0${num}` : num;
    };

    const parsedDay = dayMonthDisplayValue(day);
    const parsedMonth = dayMonthDisplayValue(month, true);

    return (
        <div className={cx("select-date")}>
            {selectLabel && (
                <div className={cx("select-date-label")} {...getTestId(`lbl[${testId}`)}>
                    {selectLabel}
                </div>
            )}
            <div {...getTestId(`btn-${testId}`)} className={cx("select-container", "multi-select")} onClick={onClick}>
                <div className={cx("date-calendar-wrapper", { active: isFocused })}>
                    <div>{`${parsedDay} ${parsedMonth} ${year}`}</div>
                    <span className="far fa-calendar" />
                </div>
            </div>
        </div>
    );
};

export default intlHoc(SelectDate);
