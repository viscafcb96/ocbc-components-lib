import classnames from "classnames";
import * as React from "react";

import { getTestId } from "../../utils/index";

import "./ChevronButton.scss";
const cx = classnames

export enum EButtonStyle {
    DARK = "dark",
    LIGHT = "light",
}

interface IChevronButton {
    testId?: string;
    style?: EButtonStyle;
    onClick?: (e: React.MouseEvent) => void;
}

const ChevronButton: React.FC<IChevronButton> = ({ testId, style = EButtonStyle.LIGHT, onClick }) => (
    <div {...getTestId(`btn-${testId}-chevron-btn`)} className={cx("menu-wrapper")} onClick={onClick}>
        <div className={cx("square", style)} />
        <div className={cx("icon", "far fa-chevron-down", style)} />
    </div>
);

export default ChevronButton;
