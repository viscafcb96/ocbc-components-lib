/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/interface-name-prefix */
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import classnames from 'classnames';
import * as React from 'react';
import intlHoc, { IWrappedComProps } from "../intlHoc"

import MenuChevron from '../chevron/Menu';
import Button from '../button/';
import TooltipWrapper from '../tooltip/Tooltip';
import './style/index.scss';

const cx = classnames;

interface IAccountHeaderProps extends IWrappedComProps {
    title?: string;
    accountNumber?: string;
    onBackClick?: () => void;
    onChevronClick?: () => void;
    onSeeFullDetails?: () => void;
    color?: string; // default is green. specify "grey" for grey
    settings?: boolean;
    mask?: boolean;
}

interface IAccountHeaderState {
    visible: boolean;
}

const ACCOUNT_NUMBER_MASK = 'XXX-XXXXXXX-XXX';

 class AccountHeader extends React.Component<
    IAccountHeaderProps & React.HTMLProps<HTMLDivElement>,
    IAccountHeaderState
> {
    state = {
        visible: false,
    };

    clickHandler = (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation();
        this.setState({ visible: false });
        enableBodyScroll(document);
    };

    onChevronClick = (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation();
        if(!this.state.visible) {
            this.setState({visible: true})
        }
    }

    componentDidMount() {
        disableBodyScroll(document);
        setTimeout(() => this.setState({ visible: true }), 1500);
        document.getElementsByTagName('body')[0].style.pointerEvents = 'none';
    }

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    componentDidUpdate(_prevProps: IAccountHeaderProps, prevState: IAccountHeaderState) {
        if (this.state.visible !== prevState.visible && !this.state.visible) {
            document.getElementsByTagName('body')[0].style.pointerEvents = null;
        }
    }

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    componentWillUnmount() {
        enableBodyScroll(document);
        document.getElementsByTagName('body')[0].style.pointerEvents = null;
    }

    render() {
        const { className, t } = this.props;

        const TooltipContent = (
            <div className={cx('account-header-tooltip')}>
                <div className={cx('account-header-tooltip__title')}>
                    {t('view-all-transactions', 'View all transactions')}
                </div>
                <div className={cx('account-header-tooltip__sub-title')}>
                    {t('select-an-account-you-own', 'Select an account you own.')}
                </div>
                <div className={cx('account-header-tooltip__cta')} onClick={this.clickHandler}>
                    {t('got-it', 'Got it')}
                </div>
            </div>
        );

        return (
            <div className={cx(['info-container', this.props.color, className])}>
                <div className={cx('info-inner')}>
                    <div className={cx('top-row')} />
                    <div className={cx('view')}>
                        {t('view', 'View')}
                    </div>
                    <div className={cx('account-container')}>
                        <div className={cx('account')}>{this.props.title}</div>
                        <div className={cx('oval')} onClick={this.props.onChevronClick}>
                            <TooltipWrapper
                                content={TooltipContent}
                                placement={'bottomRight'}
                                visible={this.state.visible}
                                classname={cx('account-header-tooltip-wrapper')}
                            >
                                <MenuChevron light={true} onClick={this.onChevronClick} />
                            </TooltipWrapper>
                        </div>
                    </div>
                    <div className={cx('account-number')}>
                        {this.props.mask ? ACCOUNT_NUMBER_MASK : this.props.accountNumber}
                    </div>
                    <Button className={cx('full-details')} light={true} onClick={this.props.onSeeFullDetails}>
                        {t('see-full-details', 'See full details')}
                    </Button>
                </div>
            </div>
        );
    }
}

export default intlHoc(AccountHeader);
