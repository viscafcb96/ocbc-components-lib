import classnames from "classnames/bind";
import * as React from "react";
import Picker from "rmc-picker";
import MultiPicker from "rmc-picker/lib/MultiPicker";
import { getTestId } from "../../utils/index";
import Button from "../button/Button";
import intlHoc, { IWrappedComProps } from '../intlHoc';

import { generateDateArray, getArrayFromDate, getValidPickerValue, MONTH } from "../../utils/helper/dateTime";

import "./DatePicker.scss";
const cx = classnames

interface IDatePickerProps extends IWrappedComProps {
    selectedValue: number;
    onValueChange: (value: number) => void;
    minimumValue?: number;
    maximumValue?: number;
    onButtonClick: () => void;
    buttonText?: string;
    testId?: string;
}

const DatePicker: React.FC<IDatePickerProps> = ({
    minimumValue,
    maximumValue,
    onValueChange,
    selectedValue,
    onButtonClick,
    buttonText,
    testId,
    t
}) => {
    const arrayDate = getArrayFromDate(selectedValue);

    const onPickerValueChange = (arrayDate: number[]) => {
        onValueChange(getValidPickerValue(arrayDate, minimumValue, maximumValue));
    };

    const onTap = (e: React.MouseEvent, tapValue: number, index: number) => {
        e.stopPropagation();
        (e.target as HTMLDivElement).parentElement!.parentElement!.classList.add(cx("animating"));
        e.persist(); // Required for React synthetic events to persist
        setTimeout(
            () => (e.target as HTMLDivElement).parentElement!.parentElement!.classList.remove(cx("animating")),
            300
        );
        const arrayDate = getArrayFromDate(selectedValue);
        arrayDate[index] = tapValue;
        onValueChange(getValidPickerValue(arrayDate, minimumValue, maximumValue));
    };

    const handleMonthWords = (parentIndex: number, item: number): string | number => {
        if (parentIndex === 1 && MONTH[item]) {
            return t(MONTH[item].short, MONTH[item].short);
        }
        return item;
    };

    return (
        <div className={cx("date-picker")}>
            <div className={cx("picker-wrapper")}>
                <div className={cx("picker-carousel")}>
                    <div className={cx("picker-mask")} />
                    <MultiPicker selectedValue={arrayDate} onValueChange={onPickerValueChange}>
                        {generateDateArray(arrayDate, minimumValue, maximumValue).map((item: number[], parentIndex) => (
                            <Picker key={`parent ${parentIndex}`}>
                                {item.map((i, childIndex) => (
                                    <Picker.Item key={`child ${childIndex}`} value={i} className={cx("picker-item")}>
                                        <div
                                            onClick={(e: React.MouseEvent) => onTap(e, i, parentIndex)}
                                            {...getTestId(`lbl-${testId}-item-${parentIndex}-${i}`)}
                                        >
                                            <span>{handleMonthWords(parentIndex, i)}</span>
                                        </div>
                                    </Picker.Item>
                                ))}
                            </Picker>
                        ))}
                    </MultiPicker>
                </div>
            </div>
            <Button
                className={cx("close-button")}
                shadow={true}
                block={true}
                onClick={onButtonClick}
                testId={`btn-close-${testId}`}
            >
                <span>{buttonText || t("done", "Done")}</span>
            </Button>
        </div>
    );
};

export default intlHoc(DatePicker);
