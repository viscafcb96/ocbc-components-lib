## When To Use

A button means an operation (or a series of operations). Clicking a button will trigger corresponding business logic.

We provide 4 types of button.

- Primary button: indicate the main action, one primary button at most in one section.
- Default button: indicate a series of actions without priority.
- LightSolid button
- Block button

## API


| Property | Description | Type | Default | Version |
| --- | --- | --- | --- | --- |
| disabled | disabled state of button | boolean | `false` |  |
| onClick | set the handler to handle `click` event | (event) => void | - |  |
| block | option to fit button width to its parent width | boolean | `false` |  |

It accepts all props which native buttons support.

## Usage

There are `primary` button, `default` button, `lightSolid` button and `disabled` button

```jsx

ReactDOM.render(
  <div>
    <Button primary>Primary</Button>
    <Button>Default</Button>
    <Button lightSolid>lightSolid</Button>
    <Button disabled>disabled</Button>
  </div>,
  mountNode,
);
```

`block` property will make the button fit to its parent width.

```jsx

ReactDOM.render(
  <div>
    <Button block >Block button</Button>;
    <Button block primary>Block button</Button>;
    <Button block lightSolid >Block button</Button>;
  </div>,
  mountNode,
);
```
