import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import * as React from 'react';
import TimePicker from './TimePicker';

export default { title: "TimePicker" }

storiesOf('TimePicker', module).add('Default', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <TimePicker selectedValue={[5, 30]} onValueChange={(value: number[]) => console.log(value)}   />
    </div>
));

storiesOf('SelectPicker', module).add('12HourFormat', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <TimePicker is12HourFormat selectedValue={[5, 30, 0]} onValueChange={(value: number[]) => console.log(value)}   />
    </div>
));