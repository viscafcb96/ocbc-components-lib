import cx from "classnames";
import React from "react";

import Button from "../button/Button";
import Dialog, { IDialogProps } from "./Dialog";
import { getTestId } from "../../utils";

import "./AlertDialog.scss";

interface IAlertDialogContentProps {
    testId?: string;
    icon?: React.ReactNode;
    title: string;
    primaryBtn?: IActionBtnProps;
    secondaryBtn?: IActionBtnProps;
    counterBtn?: IActionBtnProps;
    className?: string;
    children: React.ReactNode | string;
}

interface IActionBtnProps {
    label: string;
    onClick: () => void;
    isDisabled?: boolean;
}

export const AlertDialogContent: React.FC<IAlertDialogContentProps> = ({
    testId,
    icon,
    title,
    children,
    primaryBtn,
    secondaryBtn,
    className,
    counterBtn,
}) => (
    <div className={cx("alert-dialog")} {...getTestId(`alert-dialog-${testId}`)}>
        {icon && (
            <div className={cx("icon")} {...getTestId(`img-alert-${testId}-icon`)}>
                {icon}
            </div>
        )}
        <h3 className={cx("title")} {...getTestId(`lbl-alert-${testId}-title`)}>
            <span>{title}</span>
        </h3>
        <div className={cx("content", className, { "content-not-align": typeof children !== "string" })}>
            {children}
        </div>
        {primaryBtn && (
            <Button
                primary={true}
                block={true}
                disabled={primaryBtn.isDisabled || false}
                onClick={primaryBtn.onClick}
                {...getTestId(`btn-alert-${testId}-primary`)}
            >
                <span>{primaryBtn.label}</span>
            </Button>
        )}
        {secondaryBtn && (
            <Button
                className={cx("secondary-btn")}
                disabled={secondaryBtn.isDisabled || false}
                outline={true}
                block={true}
                onClick={secondaryBtn.onClick}
                {...getTestId(`btn-alert-${testId}-secondary`)}
            >
                <span>{secondaryBtn.label}</span>
            </Button>
        )}
        {counterBtn && (
            <button
                type="button"
                className={cx("button-link", "counter-btn")}
                disabled={counterBtn.isDisabled || false}
                onClick={counterBtn.onClick}
                {...getTestId(`btn-alert-${testId}-counter`)}
            >
                <span>{counterBtn.label}</span>
            </button>
        )}
    </div>
);

export const AlertDialog: React.FC<IDialogProps & IAlertDialogContentProps> = ({
    isOpen,
    onRequestClose,
    hasCloseBtn,
    ...contentProps
}) => (
    <Dialog isOpen={isOpen} onRequestClose={onRequestClose} hasCloseBtn={hasCloseBtn}>
        <AlertDialogContent {...contentProps} />
    </Dialog>
);

export default AlertDialog;
