import cx from "classnames";
import React, { useEffect } from "react";
import * as ReactDOM from "react-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import "./Dialog.scss";
import { getTestId } from "../../utils/index";

export interface IDialogProps {
    isOpen: boolean;
    hasCloseBtn?: boolean;
    onRequestClose?: () => void;
    testId?: string;
}

const fadeAnimation = {
    appear: 200,
    enter: 200,
    exit: 200,
};

const disabledScrollHandler = (e: Event) => {
    e.preventDefault();
    e.stopPropagation();
};

const disableScroll = () => {
    window.addEventListener("wheel", disabledScrollHandler, {
        passive: false,
    });
    window.addEventListener("touchmove", disabledScrollHandler, {
        passive: false,
    });
};

const enableScroll = () => {
    window.removeEventListener("wheel", disabledScrollHandler);
    window.removeEventListener("touchmove", disabledScrollHandler);
};

export const InternalDialog: React.FC<IDialogProps> = ({ children, isOpen, hasCloseBtn = true, onRequestClose, testId }) => {
    useEffect(() => {
        isOpen ? disableScroll() : enableScroll();
        return enableScroll;
    }, [isOpen]);

    return (
        <TransitionGroup component={null} appear={true} exit={true}>
            {isOpen && (
                <CSSTransition key={"overlay"} timeout={fadeAnimation} classNames="dialog-fade">
                    <div className={cx("dialog-overlay")}>
                        <div className={cx("dialog")}>
                            {hasCloseBtn && (
                                <button
                                    type="button"
                                    className={cx("far fa-times", "close-btn")}
                                    onClick={onRequestClose}
                                    {...getTestId(`btn-${testId}-dialog-close`)}
                                />
                            )}
                            <div className={cx("dialog-content")}>{children}</div>
                        </div>
                    </div>
                </CSSTransition>
            )}
        </TransitionGroup>
    );
};

const Dialog: React.FC<IDialogProps> = (props) =>
    ReactDOM.createPortal(<InternalDialog {...props} />, document.getElementById("dialog")!);

export default Dialog;
