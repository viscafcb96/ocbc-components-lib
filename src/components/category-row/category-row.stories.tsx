import * as React from 'react';

import CategoryRow from './CategoryRow';

export default { title: 'CategoryRow' };

export const Default = () => (
    <CategoryRow description="This is description" name="Category 1" percentage={50} image="" categoryRowAmounts={<h1>5</h1>} budgetMode></CategoryRow>
);