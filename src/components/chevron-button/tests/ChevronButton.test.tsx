import { shallow } from "enzyme";
import * as React from "react";
import ChevronButton from "../ChevronButton";

describe("ChevronButton", () => {
    it("renders the chevron icon and the square", () => {
        const template = shallow(<ChevronButton />);
        expect(template.exists()).toBe(true);
        expect(template.find(".square").length).toBe(1);
        expect(template.find(".icon").length).toBe(1);
    });
});
