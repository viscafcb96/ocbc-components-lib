import cx from "classnames";
import * as React from "react";

import { getTestId } from "../../utils/index";
import { onInputFocus } from "../../utils/index";
import "./Input.scss";

interface IInputProps {
    testId?: string;
    error?: string;
    wrapperClassName?: string;
    realPlaceholder?: string;
    refData?: React.RefObject<HTMLInputElement>;
}

interface IInputState {
    value?: string | number | string[] | undefined;
    focused?: boolean;
    maxLength?: number;
}

export default class Input extends React.Component<IInputProps & React.HTMLProps<HTMLInputElement>, IInputState> {
    state = {
        value: this.props.value,
        focused: false,
    };

    inputRef = this.props.refData || React.createRef<HTMLInputElement>();

    onChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ value: ev.target.value });
        if (typeof this.props.onChange === "function") {
            this.props.onChange.call(this, ev);
        }
    };

    onFocus = (ev: React.FocusEvent<HTMLInputElement>) => {
        this.setState({ focused: true });
        if (typeof this.props.onFocus === "function") {
            this.props.onFocus.call(this, ev);
        }
        onInputFocus(this.inputRef.current);
    };

    onBlur = (ev: React.FocusEvent<HTMLInputElement>) => {
        this.setState({ focused: false });
        if (typeof this.props.onBlur === "function") {
            this.props.onBlur.call(this, ev);
        }
    };

    render() {
        const {
            testId,
            placeholder,
            value,
            maxLength,
            error,
            wrapperClassName,
            realPlaceholder,
            refData,
            ...rest
        } = this.props;
        return (
            <div
                className={[
                    cx("input", {
                        [cx("focused")]:
                            this.state.focused === true || (this.state.value !== undefined && this.state.value !== ""),
                        [cx("error")]: this.props.error !== undefined,
                        [cx("disabled")]: this.props.disabled === true,
                        [cx("filled")]:
                            this.state.value !== undefined && this.state.value !== "" && !this.state.focused,
                    }),
                    wrapperClassName,
                ].join(" ")}
            >
                <label {...getTestId(`lbl-input-${testId}`)}>{placeholder}</label>
                <input
                    {...rest}
                    value={value}
                    onChange={this.onChange}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    ref={this.inputRef}
                    maxLength={maxLength}
                    {...getTestId(`btn-${testId!}`)}
                    placeholder={this.state.focused ? realPlaceholder : ""}
                />
                {this.props.error && (
                    <small {...getTestId(`lbl-input-error-${testId}`)} className={cx("error-label")}>
                        {this.props.error}
                    </small>
                )}
            </div>
        );
    }
}
