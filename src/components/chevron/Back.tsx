import cx from 'classnames';
import * as React from 'react';

import './style/index.scss';

interface IChevron {
    active?: boolean;
    light?: boolean;
    dark?: boolean;
    transparent?: boolean;
    onClick?: () => void;
}

export default class BackChevron extends React.Component<IChevron & React.HTMLProps<HTMLButtonElement>> {
    render() {
        const { active, light, dark, transparent, className, ...rest } = this.props;
        return (
            <button
                {...rest}
                className={cx('back', 'far fa-chevron-left', { active, light, dark, transparent }, className)}
                type="button"
                onClick={this.props.onClick}
            />
        );
    }
}
