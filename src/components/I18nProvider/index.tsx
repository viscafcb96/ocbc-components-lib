// import * as React from 'react';

// import { IntlProvider, addLocaleDat } from 'react-intl';
// import locale_en from 'react-intl/locale-data/en';
// import messages_en from './translations/en.json';

// addLocaleData([...locale_en]);

// const defaultLocale = 'en';
// const messages = {
//     en: messages_en,
// };

// // eslint-disable-next-line @typescript-eslint/interface-name-prefix
// export interface II18nProviderProps {
//     useNavigatorLocale?: boolean;
//     locale?: string;
// }

// class I18nProvider extends React.Component<II18nProviderProps, {}> {
//     render() {
//         const { useNavigatorLocale, locale, children } = this.props;
//         const language = useNavigatorLocale ? navigator.language.split(/[-_]/)[0] : locale ? locale : defaultLocale;
//         return (
//             <IntlProvider locale={language} messages={messages[language]}>
//                 {children}
//             </IntlProvider>
//         );
//     }
// }

// export default I18nProvider;
