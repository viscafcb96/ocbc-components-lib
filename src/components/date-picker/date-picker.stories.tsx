import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import * as React from 'react';
import DatePicker from './DatePicker';

export default { title: "DatePicker" }

storiesOf('DatePicker', module).add('Default', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            justifyContent: "center",
        }}
    >
        <DatePicker selectedValue={Date.now()} onValueChange={(value: number) => console.log(value)}   />
    </div>
));

// storiesOf('DatePicker', module).add('WithMinimum', () => (
//     <div
//         style={{
//             display: "flex",
//             width: "100%",
//             justifyContent: "center",
//         }}
//     >
//         <DatePicker minimumValue={1547485200000} onValueChange={(value: number) => console.log(value)}   />
//     </div>
// ));

// storiesOf('DatePicker', module).add('WithMaximum', () => (
//     <div
//         style={{
//             display: "flex",
//             width: "100%",
//             justifyContent: "center",
//         }}
//     >
//         <DatePicker selectedValue={Date.now()} onValueChange={(value: number) => console.log(value)}   />
//     </div>
// ));

