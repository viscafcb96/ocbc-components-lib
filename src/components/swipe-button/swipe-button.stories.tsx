import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import * as React from 'react';
import { action } from '@storybook/addon-actions';

import SwipeButton from './SwipeButton';

storiesOf('SwipeButton', module).add('default', () => (
    <div
        style={{
            width: '100%',
            height: '100vh',
            alignItems: 'center',
            justifyContent: 'center',
            display: 'flex',
        }}
    >
        <SwipeButton
            width={text('width', '100%')}
            text={text('text', 'Slide to convert')}
            onTouchStart={action('start swiping')}
            onEndStart={action('end swiping without success')}
            onSwiping={action('swiping')}
            onSuccessSwipe={action('end swiping with success')}
        />
    </div>
));
