import { shallow } from "enzyme";
import * as React from "react";
import Input from "../Input";

describe("Input", () => {
    it("renders the input field", () => {
        const template = shallow(<Input />);
        expect(template.exists()).toBe(true);
    });

    it("renders the placeholder as a label", () => {
        const template = shallow(<Input placeholder="label" />);
        expect(
            template
                .find("label")
                .first()
                .text()
        ).toBe("label");
    });

    it("renders the value in the input", () => {
        const template = shallow(<Input value="test" />);
        expect(
            template
                .find("input")
                .first()
                .props().value
        ).toBe("test");
    });

    it("renders the error", () => {
        const template = shallow(<Input error="test" />);
        expect(template.find("small").exists()).toBe(true);
        expect(template.find("small").text()).toBe("test");
    });

    it("renders a different input type", () => {
        const template = shallow(<Input type="password" />);
        expect(
            template
                .find("input")
                .first()
                .props().type
        ).toBe("password");
    });

    it("simulates typing", () => {
        const template = shallow(<Input />);
        template
            .find("input")
            .first()
            .simulate("change", { target: { value: "rem" } });
        const state: any = template.state();
        expect(state.value).toEqual("rem");
    });

    it("simulates an input focus", () => {
        const spy = jest.fn();
        const template = shallow(<Input onFocus={spy} />);
        template
            .find("input")
            .first()
            .simulate("focus");
        expect(spy.mock.calls.length).toEqual(1);
    });

    it("simulates an input blur", () => {
        const spy = jest.fn();
        const template = shallow(<Input onBlur={spy} />);
        template
            .find("input")
            .first()
            .simulate("blur");
        expect(spy.mock.calls.length).toEqual(1);
    });

    it("simulates an input blur", () => {
        const spy = jest.fn();
        const template = shallow(<Input onChange={spy} />);
        template
            .find("input")
            .first()
            .simulate("change", { target: { value: "rem" } });
        expect(spy.mock.calls.length).toEqual(1);
    });
});
