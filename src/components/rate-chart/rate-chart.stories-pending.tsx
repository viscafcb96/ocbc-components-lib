// import { storiesOf } from '@storybook/react';
// // import { text } from '@storybook/addon-knobs';
// import * as React from 'react';
// // import { action } from '@storybook/addon-actions';
// import moment from 'moment';
// import './RateChartWrapper.scss';
//
// import RateChart from './RateChartScreen';
//
// storiesOf('Line chart', module).add('default', () => {
//     const getRandomInt = (min: number, max: number) => {
//         min = Math.ceil(min);
//         max = Math.floor(max);
//         return Math.floor(Math.random() * (max - min + 1)) + min;
//     };
//
//     const changeRates = () => {
//         const output = getRandomInt(-999, 999) / 10000;
//         return parseFloat(output.toFixed(4));
//     };
//
//     const createMockData = (times: number, unit: string) => {
//         const values = [];
//         const xLabels = [];
//         let currentValue = 1 + changeRates();
//         for (let x = 0; x < times; x++) {
//             // @ts-ignore
//             const pointStart = moment().subtract(times - x, unit);
//             const timeText = `${pointStart.format()}`;
//             xLabels.push(timeText);
//             const valueToAdd = changeRates();
//             if (currentValue < Math.abs(valueToAdd)) {
//                 currentValue += Math.abs(valueToAdd);
//             } else {
//                 currentValue += valueToAdd;
//             }
//             currentValue = parseFloat(currentValue.toFixed(4));
//             values.push(currentValue);
//         }
//         return { values, xLabels };
//     };
//     const getDataFor1w = () => {
//         const { values, xLabels } = createMockData(24 * 7, 'hours');
//         const xTickAmount = 7;
//         const xLabelsFormat = function() {
//             // @ts-ignore
//             const position = this.pos === 0 ? this.pos : this.pos - 1;
//             // @ts-ignore
//             const value = this.chart.axes[0].categories[position];
//             return moment(value).format(`DD <br /> MMM`);
//         };
//         return { values, xLabels, xTickAmount, xLabelsFormat };
//     };
//     const getDataFor1m = () => {
//         const { values, xLabels } = createMockData(30, 'days');
//         const xTickAmount = 6;
//         const xLabelsFormat = function() {
//             // @ts-ignore
//             const position = this.pos === 0 ? this.pos : this.pos - 1;
//             // @ts-ignore
//             const value = this.chart.axes[0].categories[position];
//             return moment(value).format(`DD <br /> MMM`);
//         };
//         const tooltipFormat = function() {
//             // @ts-ignore
//             return `<div class='${cx('tooltip')}'><p>${this.y}</p><p>${moment(this.key).format(
//                 'DD MMM YYYY',
//             )}</p></div>`;
//         };
//         return { values, xLabels, xTickAmount, xLabelsFormat, tooltipFormat };
//     };
//     const getDataFor3m = () => {
//         const { values, xLabels } = createMockData(90, 'days');
//         const xTickAmount = 3;
//         const xLabelsFormat = function() {
//             // @ts-ignore
//             const position = this.pos === 0 ? this.pos : this.pos - 1;
//             // @ts-ignore
//             const value = this.chart.axes[0].categories[position];
//             return moment(value).format(`MMM <br /> YYYY`);
//         };
//         const tooltipFormat = function() {
//             // @ts-ignore
//             return `<div class='${cx('tooltip')}'><p>${this.y}</p><p>${moment(this.key).format(
//                 'DD MMM YYYY',
//             )}</p></div>`;
//         };
//         return { values, xLabels, xTickAmount, xLabelsFormat, tooltipFormat };
//     };
//     const getDataFor6m = () => {
//         const { values, xLabels } = createMockData(180, 'days');
//         const xTickAmount = 6;
//         const xLabelsFormat = function() {
//             // @ts-ignore
//             const position = this.pos === 0 ? this.pos : this.pos - 1;
//             // @ts-ignore
//             const value = this.chart.axes[0].categories[position];
//             return moment(value).format(`MMM <br /> YYYY`);
//         };
//         const tooltipFormat = function() {
//             // @ts-ignore
//             return `<div class='${cx('tooltip')}'><p>${this.y}</p><p>${moment(this.key).format(
//                 'DD MMM YYYY',
//             )}</p></div>`;
//         };
//         return { values, xLabels, xTickAmount, xLabelsFormat, tooltipFormat };
//     };
//     const getDataFor1y = () => {
//         const { values, xLabels } = createMockData(365, 'days');
//         const xTickAmount = 6;
//         const xLabelsFormat = function() {
//             // @ts-ignore
//             const position = this.pos === 0 ? this.pos : this.pos - 1;
//             // @ts-ignore
//             const value = this.chart.axes[0].categories[position];
//             return moment(value).format(`MMM <br /> YYYY`);
//         };
//         const tooltipFormat = function() {
//             // @ts-ignore
//             return `<div class='${cx('tooltip')}'><p>${this.y}</p><p>${moment(this.key).format(
//                 'DD MMM YYYY',
//             )}</p></div>`;
//         };
//         return { values, xLabels, xTickAmount, xLabelsFormat, tooltipFormat };
//     };
//     const charts = [
//         // { value: "1d", label: "1D", ...getDataFor1d() },
//         { value: '1w', label: '1W', ...getDataFor1w() },
//         { value: '1m', label: '1M', ...getDataFor1m() },
//         { value: '3m', label: '3M', ...getDataFor3m() },
//         { value: '6m', label: '6M', ...getDataFor6m() },
//         { value: '1y', label: '1Y', ...getDataFor1y() },
//     ];
//     const currentChartSelected = 0;
//     return (
//         <div className="line-chart-wrapper">
//             <RateChart
//                 key={charts[currentChartSelected].value}
//                 xLabels={charts[currentChartSelected].xLabels}
//                 values={charts[currentChartSelected].values}
//                 xTickAmount={charts[currentChartSelected].xTickAmount}
//                 tooltipFormat={charts[currentChartSelected].tooltipFormat}
//                 xLabelsFormat={charts[currentChartSelected].xLabelsFormat}
//             />
//         </div>
//     );
// });
