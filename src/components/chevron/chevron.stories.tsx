import * as React from 'react';
import Back from './Back';
import Down from './Down';
import Forward from './Forward';
import ChevronMenu from './Menu';
import ChevronView from './Chevron';

export default { title: 'Chevron' };

export const Menu = () => (
    <ChevronMenu></ChevronMenu>
);

export const ForwardButton = () => <Forward></Forward>;

export const DownButton = () => <Down></Down>;

export const BackButton = () => <Back></Back>;
export const Chevron = () => <ChevronView></ChevronView>;
