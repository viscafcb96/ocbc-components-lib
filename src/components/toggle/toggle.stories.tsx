import * as React from 'react';

import Toogle from "./Toggle";

export default { title: 'Toogle' };


export const Default = () => (
    <Toogle checked={true} onHandleChange={(checked: boolean) => console.log('checked', checked)} />
);

export const Disabled = () => (
    <Toogle disabled checked={true} onHandleChange={(checked: boolean) => console.log('checked', checked)} />
);
