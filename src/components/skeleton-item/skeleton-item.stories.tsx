import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import * as React from 'react';

import SkeletonItem from './SkeletonItem';

storiesOf('SkeletonItem', module).add('default', () => (
    <div
        style={{
            display: "flex",
            width: "100%",
            margin: 'auto',
            height: 100,
        }}
    >
        <SkeletonItem
            width={text('width', '225px')}
            height={text('heght', '225px')}
            mr={10}
            dark
        />
        <SkeletonItem
            width={text('width', '225px')}
            height={text('heght', '225px')}
            mr={10}
            dark
        />
        <SkeletonItem
            width={text('width', '225px')}
            height={text('heght', '225px')}
            mr={10}
            dark
        />
        <SkeletonItem
            width={text('width', '225px')}
            height={text('heght', '225px')}
            mr={10}
            dark
        />
    </div>
));
