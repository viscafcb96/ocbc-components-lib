const currencyFactor = 0.05;

export const moneyDisplayFormat = (num: number, precision = 2): string => {
    if (isNaN(num)) {
        throw new Error('Invalid number format for money');
    }
    const arrDollarCents = num.toFixed(precision).split('.');
    const formattedDollars = arrDollarCents[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    if (precision === 0) {
        return formattedDollars;
    }
    return `${formattedDollars}.${(arrDollarCents[1] &&
        (arrDollarCents[1].toString().length >= precision
            ? arrDollarCents[1]
            : arrDollarCents[1] + '0'.repeat(precision - arrDollarCents[1].length))) ||
        '0'.repeat(precision)}`;
};

export const percentDisplayFormat = (num: number, precision = 2): string => {
    if (isNaN(num)) {
        throw new Error('invalid-number-format-for-percent');
    }
    const arrIntFloat = num.toFixed(precision).split('.');
    return `${arrIntFloat[0]}.${(arrIntFloat[1] &&
        (arrIntFloat[1].toString().length >= precision
            ? arrIntFloat[1]
            : arrIntFloat[1] + '0'.repeat(precision - arrIntFloat[1].length))) ||
        '0'.repeat(precision)}`;
};

export const getValidAmount = (amount: number) =>
    parseFloat((Math.round(amount / currencyFactor) * currencyFactor).toFixed(2));

export const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

// Capitalizes first character of each word
export const capitalizeAll = (str: string) =>
    str
        .split(' ')
        .map(word => capitalize(word))
        .join(' ');
