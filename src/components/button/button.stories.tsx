import * as React from 'react';
import { storiesOf, } from '@storybook/react';
import { radios, text } from '@storybook/addon-knobs';
import Button from "./index";
import ButtonReadme from "./Button.md";


export default { title: 'Button' };


const label = 'Props';
const options = {
    disabled: 'disabled',
    primary: 'primary',
    block: 'block',
    lightSolid: 'lightSolid',
    light: 'light',
    shadow: 'shadow',
    outline: 'outline',
    active: 'active'
};
const defaultValue = 'primary';
const groupId = 'Props';
console.log(radios(label, options, defaultValue, groupId))

const onClick = (message: string) => () => console.log(message);

storiesOf('Button', module)
    .addParameters({
        readme: {
            sidebar: ButtonReadme,
            codeTheme: 'github',
        },
    })
    .add('Default', () => {
        const props = { [radios(label, options, defaultValue, groupId)]: true }
        return <div
            style={{
                width: "100%",
            }}
        >
            <Button onClick={onClick('Button')} {...props} >{text('text', 'Button')}</Button>
        </div>
    })


// export const Default = () => <Button onClick={onClick('Default Button')}>Default Button</Button>;

// export const Disabled = () => <Button disabled onClick={onClick('Disabled Button')}>Disabled Button</Button>;

// export const Block = () => <Button block onClick={onClick('Block button')}>Block button</Button>;

// export const LightSolid = () => <Button lightSolid onClick={onClick('LightSolid Button')}>LightSolid Button</Button>;

// export const Primary = () => {
//     return <Button primary onClick={onClick('Primary Button')}>Primary Button</Button>;
// }