import { mount, shallow } from "enzyme";
import React from "react";

import { InternalDialog } from "../Dialog";

describe("Dialog", () => {
    it("render component", () => {
        const tpl = mount(
            <InternalDialog isOpen={true}>
                <div className="hello" />
            </InternalDialog>
        );

        expect(tpl.exists()).toBe(true);
    });

    it("render children", () => {
        const tpl = shallow(
            <InternalDialog isOpen={true}>
                <div className="hello" />
            </InternalDialog>
        );

        expect(tpl.find(".hello").exists()).toBe(true);
    });

    it("calls onRequestClose event handler when clicking close button", () => {
        const mockHandleOnRequestClose = jest.fn();
        const tpl = mount(
            <InternalDialog isOpen={true} onRequestClose={mockHandleOnRequestClose}>
                <div className="hello" />
            </InternalDialog>
        );

        tpl.find("button")
            .first()
            .simulate("click");

        expect(mockHandleOnRequestClose).toHaveBeenCalled();
    });
});
