import classnames from "classnames";
import * as React from "react";

import { getTestId } from "../../utils/index";

import  "./CardSummary.scss";
const cx = classnames

interface ICardSummaryProps {
    testId?: string;
    status?: string;
    icon: string;
    cardName: string;
    cardNumber: string;
    description?: string;
    locked?: boolean;
}

const CardSummary: React.FC<ICardSummaryProps> = ({
    testId,
    status,
    icon,
    cardName,
    cardNumber,
    description,
    locked,
}: ICardSummaryProps) => (
    <div className={cx("card-summary")}>
        {status && (
            <span {...getTestId(`lbl-card-summary-${testId}-status-icon`)} className={cx("status", status)}>
                <i
                    className={cx("far", {
                        "fa-check": status === "success",
                        "fa-times": status === "error",
                    })}
                />
            </span>
        )}
        {icon && (
            <span className={cx("image", { locked })}>
                <img
                    {...getTestId(`img-card-summary-${testId}-card-icon`)}
                    className={cx("icon")}
                    src={icon}
                    title={cardName}
                />
            </span>
        )}
        <div className={cx("name-number")}>
            <h6 {...getTestId(`lbl-card-summary-${testId}-card-name`)}>
                <span>{cardName}</span>
            </h6>
            <p {...getTestId(`lbl-card-summary-${testId}-card-number`)} className={cx("small-regular", "card-number")}>
                <span>{cardNumber}</span>
            </p>
            {description && (
                <p
                    {...getTestId(`lbl-card-summary-${testId}-card-desc`)}
                    className={cx("small-semi-bold", "description")}
                >
                    <span>{description}</span>
                </p>
            )}
        </div>
    </div>
);

export default CardSummary;
