/* eslint-disable @typescript-eslint/no-magic-numbers */
import moment from "moment";

export const isNightMode = () => {
    const currentDate = new Date();
    const afterNight = (currentDate.getHours() === 21 && currentDate.getMinutes() >= 30) || currentDate.getHours() > 21;
    const beforeMorning = currentDate.getHours() < 6;
    return afterNight || beforeMorning;
};

export const MONTH = [
    { short: "jan", long: "january", num: "01" },
    { short: "feb", long: "february", num: "02" },
    { short: "mar", long: "march", num: "03" },
    { short: "apr", long: "april", num: "04" },
    { short: "may", long: "may", num: "05" },
    { short: "jun", long: "june", num: "06" },
    { short: "jul", long: "july", num: "07" },
    { short: "aug", long: "august", num: "08" },
    { short: "sep", long: "september", num: "09" },
    { short: "oct", long: "october", num: "10" },
    { short: "nov", long: "november", num: "11" },
    { short: "dec", long: "december", num: "12" },
];

export enum Month {
    JAN = 0,
    FEB = 1,
    MAR = 2,
    APR = 3,
    MAY = 4,
    JUN = 5,
    JUL = 6,
    AUG = 7,
    SEP = 8,
    OCT = 9,
    NOV = 10,
    DEC = 11,
}

export const daysInMonth = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
];

export interface ITimer {
    hours?: number;
    minutes?: number;
    meridiem?: Meridiem;
}

export enum Meridiem {
    AM = 0,
    PM = 1,
}

export interface IMonthYear {
    month: number;
    year: number;
}

const numberOfDaysInTypicalMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const leapYearFebruaryDays = 29;
const indexAdjustValue = 1;

const has31Days = (month: number) => numberOfDaysInTypicalMonth[month] === 31;

const isLeapYear = (year: number) => {
    if (year % 100 === 0) {
        return year % 400 === 0;
    }
    return year % 4 === 0;
};

export const getDaysInAMonth = (month: number, year?: number) => {
    if (year && isLeapYear(year) && month === Month.FEB) {
        return leapYearFebruaryDays;
    }
    return numberOfDaysInTypicalMonth[month];
};

const parseDateBeforeTenth = (day: number): string => (day < 10 ? `0${day}` : `${day}`);

export const dateStringShortMonth = (jsTime: number) => {
    const date = new Date(jsTime);
    const day = date.getDate();
    const month = date.getMonth();

    const parsedDay = parseDateBeforeTenth(day);
    const parsedMonth = MONTH[month].short;

    return `${parsedDay} ${parsedMonth}`;
};

export const dateTimeString = (minuteOffset: number) => {
    const dateNow = new Date();
    dateNow.setMinutes(dateNow.getMinutes() + Math.floor(minuteOffset));

    const day = dateNow.getDate();
    const parsedDay = parseDateBeforeTenth(day);

    const month = dateNow.getMonth();
    const parsedMonth = MONTH[month].short;

    const year = dateNow.getFullYear();

    const dateString = `${parsedDay} ${parsedMonth} ${year}`;
    const timeString = dateNow.toLocaleString([], { hour: "numeric", minute: "numeric", hour12: true });

    return `${dateString}, ${timeString.toUpperCase()}`;
};

export const isValidDate = (date: number[]): boolean => {
    const [dayNumber, monthNumber, yearNumber] = date;
    if (dayNumber < 1 || dayNumber > 31) {
        return false;
    }

    if (
        (monthNumber !== Month.FEB && dayNumber <= 30) ||
        (monthNumber === Month.FEB && dayNumber <= getDaysInAMonth(Month.FEB))
    ) {
        return true;
    }

    if (monthNumber === Month.FEB && dayNumber === leapYearFebruaryDays) {
        return isLeapYear(yearNumber);
    }

    return has31Days(monthNumber);
};

export const getFutureDateDays = (days: number, jsDate: number | null = null): number => {
    const date = jsDate ? new Date(jsDate) : new Date();
    date.setDate(date.getDate() + days);
    return Date.parse(date.toString());
};

export const getFutureDateYears = (year: number) => {
    const date = new Date();
    date.setFullYear(date.getFullYear() + year);
    return Date.parse(date.toString());
};

export const getPastDateDays = (days: number) => {
    const date = new Date();
    date.setDate(date.getDate() - days);
    return Date.parse(date.toString());
};

export const getArrayFromDate = (dateNumber: number): number[] => {
    if (dateNumber === 0) {
        return [NaN, NaN, NaN];
    }

    const date = new Date(dateNumber);
    const dd = date.getDate();
    const mm = date.getMonth();
    const yyyy = date.getFullYear();
    return [dd, mm, yyyy];
};

export const getDateFromArray = (arrayDate: number[]): number =>
    Date.parse(new Date(arrayDate[2], arrayDate[1], arrayDate[0]).toString());

const computeMinimumDayMonth = (
    minimumDate: number,
    inputDay: number,
    inputMonth: number,
    inputYear: number
): { day: number; month: number } => {
    const [minimumDay, minimumMonth] = getArrayFromDate(minimumDate);

    if (inputMonth < minimumMonth) {
        const computedDate = getDateFromArray([inputDay, minimumMonth, inputYear]);

        return computedDate < minimumDate
            ? { day: minimumDay, month: minimumMonth }
            : { day: inputDay, month: minimumMonth };
    } else if (inputDay < minimumDay) {
        return { day: minimumDay, month: inputMonth };
    }

    return { day: inputDay, month: inputMonth };
};

const computeMaximumDayMonth = (
    maximumDate: number,
    inputDay: number,
    inputMonth: number,
    inputYear: number
): { day: number; month: number } => {
    const [maximumDay, maximumMonth] = getArrayFromDate(maximumDate);

    if (inputMonth > maximumMonth) {
        const computedDate = getDateFromArray([inputDay, maximumMonth, inputYear]);

        return computedDate > maximumDate
            ? { day: maximumDay, month: maximumMonth }
            : { day: inputDay, month: maximumMonth };
    } else if (inputDay > maximumDay) {
        return { day: maximumDay, month: inputMonth };
    }

    return { day: inputDay, month: inputMonth };
};

export const getValidPickerValue = (
    inputDateArray: number[],
    minValue: number = getFutureDateDays(0),
    maxValue: number = getFutureDateYears(50)
) => {
    const [inputDay, inputMonth, inputYear] = inputDateArray;
    const inputDate = getDateFromArray(inputDateArray);

    let resDay = inputDay;
    let resMonth = inputMonth;
    const resYear = inputYear;

    if (inputDate < minValue) {
        const { day, month } = computeMinimumDayMonth(minValue, inputDay, inputMonth, inputYear);

        resDay = day;
        resMonth = month;
    } else if (inputDate > maxValue) {
        const { day, month } = computeMaximumDayMonth(maxValue, inputDay, inputMonth, inputYear);

        resDay = day;
        resMonth = month;
    }

    // handles scenarios of tapping from big months to small months
    // 30th March -> tap on month -> 30th Feb
    while (!isValidDate([resDay, resMonth, resYear])) {
        resDay -= 1;

        if (resDay < 1) {
            return getDateFromArray([1, resMonth, resYear]);
        }
    }

    return getDateFromArray([resDay, resMonth, resYear]);
};

export const getValidMonthYearPickerValue = (
    inputArray: number[],
    minMonth: number,
    maxMonth: number,
    minYear: number,
    maxYear: number
) => {
    const [inputMonth, inputYear] = inputArray;
    let resMonth = inputMonth;

    if (minYear === inputYear && inputMonth < minMonth) {
        resMonth = minMonth;
    }

    if (maxYear === inputYear && inputMonth > maxMonth) {
        resMonth = maxMonth;
    }

    return [resMonth, inputYear];
};

export const generateArray = (arrayLength: number, startingNumber: number) => {
    return new Array(arrayLength).fill(0).map((_e: number, index: number) => startingNumber + index);
};

export const generateDateArray = (
    arrayDate: number[],
    minValue: number = getFutureDateDays(0),
    maxValue: number = getFutureDateYears(50)
): number[][] => {
    const [inputDay, inputMonth, inputYear] = arrayDate;
    const [minDay, minMonth, minYear] = getArrayFromDate(minValue);
    const [maxDay, maxMonth, maxYear] = getArrayFromDate(maxValue);

    let dayArray = generateArray(getDaysInAMonth(inputMonth, inputYear), 1);
    let monthArray = generateArray(12, 0);
    const yearArray = generateArray(maxYear - minYear + indexAdjustValue, minYear);

    if (minYear === maxYear) {
        monthArray = generateArray(maxMonth - minMonth + indexAdjustValue, minMonth);
        if (minMonth === maxMonth) {
            dayArray = generateArray(maxDay - minDay + indexAdjustValue, minDay);
        } else {
            if (inputMonth === minMonth) {
                dayArray = generateArray(getDaysInAMonth(inputMonth, inputYear) - minDay + indexAdjustValue, minDay);
            }
            if (inputMonth === maxMonth) {
                dayArray = generateArray(maxDay, 1);
            }
        }
    } else {
        if (inputYear === minYear) {
            monthArray = generateArray(12 - minMonth, minMonth);
            if (inputMonth === minMonth) {
                dayArray = generateArray(getDaysInAMonth(inputMonth, inputYear) - minDay + indexAdjustValue, minDay);
            }
        }

        if (inputYear === maxYear) {
            monthArray = generateArray(maxMonth + indexAdjustValue, 0);
            if (inputMonth === maxMonth) {
                dayArray = generateArray(maxDay, 1);
            }
        }
    }

    return [dayArray, monthArray, yearArray];
};

export const generateMonthYearArray = (
    arrayMonthYear: number[],
    minMonth: number,
    maxMonth: number,
    minYear: number,
    maxYear: number
): number[][] => {
    const [inputMonth, inputYear] = arrayMonthYear;

    let monthArray = generateArray(12, 0);
    const yearArray = generateArray(maxYear - minYear + indexAdjustValue, minYear);

    if (minYear === maxYear) {
        monthArray = generateArray(maxMonth - minMonth + indexAdjustValue, minMonth);
    } else {
        if (inputYear === minYear) {
            monthArray = generateArray(12 - minMonth, minMonth);
        }

        if (inputYear === maxYear) {
            monthArray = generateArray(maxMonth + indexAdjustValue, 0);
        }
    }

    return [monthArray, yearArray];
};

export const durationFromNow = (endDate: string) => {
    const today = moment();
    const endDay = moment(endDate, "YYYY-MM-DD");

    if (!endDay.isValid() || endDay < today) {
        return false;
    }

    const diffInMonths = endDay.diff(today, "months");
    if (diffInMonths > 0) {
        return `${diffInMonths} ${diffInMonths > 1 ? "months" : "month"} left`;
    }

    const diffInWeeks = endDay.diff(today, "weeks");
    if (diffInWeeks > 0) {
        return `${diffInWeeks} ${diffInWeeks > 1 ? "weeks" : "week"} left`;
    }

    const diffInDays = endDay.diff(today, "days");
    return `${diffInDays} ${diffInDays > 1 ? "days" : "day"} left`;
};

export const formatDDMMMYYYY = (date: string, orginalFormat?: string) =>
    moment(date, orginalFormat).format("DD MMM YYYY");

// TIME PICKER
export const generateTimeArray = (is12HourFormat = false) => {
    const hoursArray = generateArray(is12HourFormat ? 12 : 24, is12HourFormat ? 1 : 0);
    const minutesArray = generateArray(60, 0);
    const meridiem = [Meridiem.AM, Meridiem.PM];
    return is12HourFormat ? [hoursArray, minutesArray, meridiem] : [hoursArray, minutesArray];
};

export const getArrayFromTime = (selectedValue: ITimer, is12HourFormat = false) => {
    if (!selectedValue || Object.keys(selectedValue).length < 1) {
        return [];
    }
    const defaultHoursSelected = is12HourFormat ? 1 : 0;
    return selectedValue
        ? [selectedValue.hours || 0, selectedValue.minutes || 0, selectedValue.meridiem || 0]
        : [defaultHoursSelected, 0, Meridiem.AM || 0];
};

export const getValidTimePickerValue = (
    inputTimeArray: number[],
    selectedInputArray: number[],
    is12HourFormat = false,
    index: number
) => {
    if (is12HourFormat) {
        if (index === 0 && inputTimeArray[0] === 12) {
            // When switch to 12, toggle medidian
            inputTimeArray[2] = Meridiem.PM;
        } else if (index === 0 && inputTimeArray[0] < 12 && selectedInputArray[0] === 12) {
            // If hours moved down from 12, change Meridiem
            inputTimeArray[2] = Meridiem.AM;
        }
    } else if (!is12HourFormat && (index === 0 || index === 1)) {
        // Hours or Minutes
        inputTimeArray[2] = inputTimeArray[0] >= 12 ? Meridiem.PM : Meridiem.AM;
    }
    return getArrayFromTime({ hours: inputTimeArray[0], minutes: inputTimeArray[1], meridiem: inputTimeArray[2] });
};

export const formatDisplayTime = (timerValue: number, parentIndex: number, is12HourFormat = false) => {
    if (is12HourFormat) {
        if (parentIndex === 0 && (timerValue < 1 || timerValue > 12)) {
            // Hours
            timerValue = 1;
        }
    } else {
        if (parentIndex === 0 && (timerValue < 0 || timerValue > 23)) {
            // Hours
            timerValue = 0;
        }
    }
    if (
        (parentIndex === 1 && (timerValue < 0 || timerValue > 59)) ||
        (parentIndex === 2 && (timerValue < 0 || timerValue > 1))
    ) {
        // minutes OR Meridiem
        timerValue = 0;
    }

    if ((parentIndex === 0 && !is12HourFormat) || parentIndex === 1) {
        // Hours or minutes
        return timerValue < 10 ? `0${timerValue}` : timerValue;
    } else if (parentIndex === 2) {
        // Meridiem
        return timerValue === 1 ? "PM" : "AM";
    }
    return timerValue;
};

export const addOrdinalSuffix = (date: number) => {
    const array = ` ${date}`.split("").reverse();
    // Excluding 11, 12, 13
    if (array[1] != "1") {
        switch (array[0]) {
            case "1":
                return `${date}st`;
            case "2":
                return `${date}nd`;
            case "3":
                return `${date}rd`;
        }
    }

    return `${date}th`;
};

export const getMonthFromResetDay = (resetDay: number, month: number, year: number) => {
    const DECEMBER = 11;
    const endPeriodMonth = month === DECEMBER ? 0 : month + 1;
    const endPeriodYear = month === DECEMBER ? year + 1 : year;

    const daysStartPeriodMonth = getDaysInAMonth(month, year);
    const daysEndPeriodMonth = getDaysInAMonth(endPeriodMonth, endPeriodYear);

    // Start period date is default to resetDay; if the resetDay doesn't exist in the month, use the next last date of the month
    // e.g. if resetDay is 31 but February only has 28 days, use 28
    const startPeriodDay = resetDay > getDaysInAMonth(month, year) ? getDaysInAMonth(month, year) : resetDay;

    // End period date is always a day before the next reset day
    const endPeriodDay =
        resetDay - 1 > getDaysInAMonth(endPeriodMonth, endPeriodYear)
            ? getDaysInAMonth(endPeriodMonth, endPeriodYear) - 1
            : resetDay - 1;

    const startDate = {
        day: startPeriodDay,
        month: month,
        year: year,
    };

    switch (resetDay) {
        case 1:
            return {
                startDate: startDate,
                endDate: {
                    day: daysStartPeriodMonth,
                    month: month,
                    year: year,
                },
            };
        case 31:
            return {
                startDate: startDate,
                endDate: {
                    day: endPeriodDay === daysEndPeriodMonth ? endPeriodDay - 1 : endPeriodDay,
                    month: endPeriodMonth,
                    year: endPeriodYear,
                },
            };
        default:
            return {
                startDate: startDate,
                endDate: {
                    day: endPeriodDay,
                    month: endPeriodMonth,
                    year: endPeriodYear,
                },
            };
    }
};
