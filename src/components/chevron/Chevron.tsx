import cx from 'classnames';
import * as React from 'react';
import Menu from './Menu';
import Down from './Down';
import Back from './Back';
import Forward from './Forward';
import './style/index.scss';

// import { getTestId } from '../../utils/app-utils';

export enum EChevronDirection {
    UP = 'up',
    DOWN = 'down',
    LEFT = 'left',
    RIGHT = 'right',
}

export enum EChevronColour {
    GREY = '#6B8592',
    BLUE = '#005EFD',
}

interface IChevronProps {
    direction?: EChevronDirection;
    colour?: EChevronColour;
    testId?: string;
}

class Chevron extends React.Component<IChevronProps, {}> {
    static Menu: typeof Menu;
    static Down: typeof Down;
    static Back: typeof Back;
    static Forward: typeof Forward;
    render() {
        const { direction = EChevronDirection.RIGHT, colour = EChevronColour.GREY, testId } = this.props;
        return (
            <div className={cx('chevron-container', direction)}>
                <svg
                    className={cx('chevron-svg')}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    style={{ fill: colour }}
                >
                    <defs>
                        <path
                            id="a"
                            d="M15.574 12.281l-5.168 5.203a.42.42 0 01-.299.141.49.49 0 01-.298-.105l-.704-.704A.514.514 0 019 16.5a.38.38 0 01.105-.281L13.29 12 9.105 7.781A.38.38 0 019 7.5c0-.117.035-.223.105-.316l.704-.704a.49.49 0 01.298-.105.49.49 0 01.3.105l5.167 5.204c.07.093.106.199.106.316a.514.514 0 01-.106.316v-.035z"
                        />
                    </defs>
                    <g fillRule="evenodd" stroke="none" strokeWidth="1">
                        <mask id="b">
                            <use xlinkHref="#a" />
                        </mask>
                        <use fillRule="nonzero" xlinkHref="#a" />
                        <g mask="url(#b)">
                            <path d="M0 0H24V24H0z" />
                        </g>
                        <path d="M0 0L24 0 24 24 0 24z" mask="url(#b)" />
                    </g>
                </svg>
            </div>
        );
    }
}

export default Chevron;
