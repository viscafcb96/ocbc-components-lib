import { shallow } from "enzyme";
import * as React from "react";
import AlertMessage from "../AlertMessage";

describe("Donut", () => {
    it("renders the AlertMessage", () => {
        const template = shallow(<AlertMessage>Hello world!</AlertMessage>);
        expect(template.exists()).toBe(true);
    });
});
